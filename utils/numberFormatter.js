const numberFormatter = (number) => {
  if (number > 1000000000) {
    return (number / 1000000000).toString() + "тэрбум";
  } else if (number > 1000000) {
    return (number / 1000000).toString() + "сая";
  } else if (number > 1000) {
    return (number / 1000).toString() + "K";
  } else {
    return number.toString();
  }
};
export default numberFormatter;
