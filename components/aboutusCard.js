import React from "react";
/* eslint-disable @next/next/no-img-element */

import Image from "next/image";
import Link from "next/link";
import style from "@/styles/background.module.css";
import { ActiveBtnDot } from "@/lib/btn";

const AboutusCard = ({
  id = 0,
  btn = "",
  name = "",
  img,
  job = "",
  age = "",
  locale = "mn",
  lastname = "",
  slug = "",
}) => {
  return (
    <div className="w-[1/5] lg:h-[350px] md:h-[300px] h-[250px] relative rounded-lg overflow-hidden group bg-slate-800 ">
      <Image
        alt={name}
        fill
        // width={861}
        // height={1026}
        placeholder="blur"
        src={`/team${img.high}`}
        blurDataURL={`/team${img.low}`}
        // style={{ objectFit: "fisll" }}
        className="group-hover:scale-125 transition-all ease-in-out object-cover"
      />

      <div className={style.gradientImg} />
      <div className="absolute bottom-0 right-0 left-0 text-left px-4 pb-4 text-white ">
        <div>
          {locale === "mn" ? (
            <p className="sm:text-xl text-base capitalize font-semibold">
              {lastname.charAt(0)}. {name}
            </p>
          ) : (
            <p className="sm:text-xl text-base capitalize font-semibold">
              {name} .{lastname.charAt(0)}
            </p>
          )}

          <p className="sm:text-base text-base">{job}</p>
        </div>
        <div className="relative w-fit">
          <div
            // onClick={() => console.log("Clicked")}
            className="rounded px-2 py-1 mt-2 border border-sky-700 group-hover:bg-sky-500 transition-all ease-in-out"
          >
            {id === 0 && <ActiveBtnDot />}

            <p className="text-sm text-sky-300 font-semibold  group-hover:text-white">
              <Link href={`/aboutus/${id}`}>
                {locale === "mn" ? "Дэлгэрэнгүй" : "More"}
              </Link>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AboutusCard;

export function IdSliderCard({
  id = 0,
  btn = "",
  name = "",
  img,
  job = "",
  lastname = "",
  age = "",
  locale = "mn",
  slug = "",
}) {
  return (
    <div
      // onClick={() => handleClick(id)}
      className="relative w-full h-full rounded-lg overflow-hidden group "
    >
      <Image
        alt={name}
        layout="fill"
        placeholder="blur"
        objectFit="cover"
        src={`/team${img.high}`}
        blurDataURL={`/team${img.low}`}
        className="group-hover:scale-125 transition-all ease-in-out"
      />
      <div className={style.gradientImg} />
      <div className="px-4 absolute bottom-0 right-0 left-0 text-left pb-4 text-white">
        <div>
          {/* <p className="sm:text-xl text-base capitalize font-semibold">{name}</p> */}
          {locale === "mn" ? (
            <p className="sm:text-xl text-base capitalize font-semibold">
              {lastname.charAt(0)}. {name}
            </p>
          ) : (
            <p className="sm:text-xl text-base capitalize font-semibold">
              {name} .{lastname.charAt(0)}
            </p>
          )}
          <p className="sm:text-base text-base">{job}</p>
        </div>
        {/* <button
          onClick={() => {
            console.log("clicking...");
            handleClick(id);
          }}
          className="z-50 text-base text-sky-500 border-2 border-sky-500 rounded-md px-2 py-[1px] mt-1"
        >
          {btnName}
        </button> */}
      </div>
    </div>
  );
}
