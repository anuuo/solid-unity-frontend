import React from "react";
import Link from "next/link";

import { BsChevronDoubleDown } from "react-icons/bs";
import { Title } from "@/lib/title";

import Particles from "react-tsparticles";
import { loadFull } from "tsparticles";
import Image from "next/image";
import { useTranslation } from "next-i18next";
import { useDimensions } from "@/hooks/index";

// import BlurredBuildingImg from "@/public/header/low/cube.jpeg";
import { mergeNames } from "../utils";

const particleOption = {
  fpsLimit: 120,
  particles: {
    color: {
      value: "#ffffff",
    },
    links: {
      color: "#ffffff",
      distance: 160,
      enable: true,
      opacity: 0.5,
      width: 1,
    },
    collisions: {
      enable: true,
    },
    move: {
      directions: "none",
      enable: true,
      outModes: {
        default: "bounce",
      },
      random: false,
      speed: 1,
      straight: false,
    },
    number: {
      density: {
        enable: true,
        area: 850,
      },
      value: 80,
    },
    opacity: {
      value: 0.5,
    },
    shape: {
      type: "circle",
    },
    size: {
      value: { min: 1, max: 5 },
    },
  },
  detectRetina: true,
};

const Landingpage = ({ locale }) => {
  const { t } = useTranslation("home");
  const { height } = useDimensions();
  const [scrollY, setScrollY] = React.useState(0);

  const particlesInit = React.useCallback(async (engine) => {
    console.debug(engine);
    await loadFull(engine);
  }, []);

  const particlesLoaded = React.useCallback(async (container) => {
    await console.debug(container);
  }, []);

  React.useEffect(() => {
    function logit() {
      setScrollY(window.pageYOffset);
    }
    function watchScroll() {
      window.addEventListener("scroll", logit);
    }
    watchScroll();
    return () => {
      window.removeEventListener("scroll", logit);
    };
  }, []);

  const showParticle = React.useMemo(() => {
    if (scrollY > height) {
      return false;
    }
    return true;
  }, [scrollY, height]);

  return (
    <div className="relative w-full h-full bg-center bg-cover">
      <div>
        <div
          className={mergeNames(
            "fixed top-0 bottom-0 overflow-scroll w-screen h-screen",
            "bg-[url('/header/low/cube.jpeg')] bg-no-repeat bg-cover bg-center",
            "bg-black"
          )}
        >
          {/* <img
            alt="img"
            src={BlurredBuildingImg.src}
            className="object-cover h-screen w-screen fixed"
          /> */}
          <Image
            // layout="fill"
            // width="100vw"
            // height="100vh"
            // objectFit="cover"
            // objectPosition={"50%"}
            fill
            alt="team"
            sizes="100vw"
            placeholder="blur"
            src="/header/high/cube.jpeg"
            blurDataURL="/header/low/cube.jpeg"
            style={{ objectFit: "cover", objectPosition: "50%" }}
          />

          {showParticle && (
            <Particles
              id="tsparticles"
              init={particlesInit}
              loaded={particlesLoaded}
              options={particleOption}
            />
          )}
        </div>
        <div
          className={mergeNames(
            "h-screen w-screen relative",
            "bg-gradient-to-b from-black/50 to-bgdark/70"
          )}
        >
          <div className="flex flex-col items-center w-full h-full">
            <div className="flex-1 flex flex-col justify-center items-center w-full mt-36">
              <Title>{t("cpname")}</Title>
              <div className=" w-3/5 text-center text-base md:text-base font-light text-gray-100">
                <p className="pb-8 md:text-lg text-base">{t("desc")}</p>
                <Link href="/quotation">
                  <span className="px-4 py-3 sm:text-lg text-base font-medium text-white bg-sky-600 transition ease-in-out shadow rounded-full hover:bg-teal-600 hover:scale-105">
                    {locale === "mn"
                      ? "Үнийн санал авах"
                      : "Request for quotation"}
                  </span>
                </Link>
              </div>
            </div>
            <div className="pb-2">
              {/* <p className="text-base pb-1">Scroll</p> */}
              <BsChevronDoubleDown
                size={40}
                className="animate-bounce text-gray-200"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Landingpage;
