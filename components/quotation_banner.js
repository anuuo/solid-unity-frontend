import React from "react";
import low from "@/public/team/low/group_up.jpeg";
import { SubTitleWhite } from "@/lib/title";
import Image from "next/image";
import { ActiveBtnDotQuo } from "@/lib/btn";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";

const QuotationBanner = ({ locale }) => {
  const router = useRouter();
  const { t } = useTranslation("quotation");

  const handleClick = () => {
    router.push("/quotation");
  };
  return (
    <div className="w-full max-h-[90vh] min-h-[80vh] lg:px-60 md:px-40 sm:px-20 px-10 py-24 text-slate-200 flex sm:flex-row flex-col items-center justify-center gap-10 text-left">
      <div className="sm:w-2/3 w-full flex flex-col justify-center items-start">
        <div className="text-left">
          <SubTitleWhite>{t("question")}</SubTitleWhite>
        </div>
        <p className="text-slate-400 text-xl">{t("desc")}</p>
        <button
          onClick={handleClick}
          className="rounded-full sm:px-8 sm:py-3 px-4 py-[10px] bg-sky-500 mt-10 relative hover:bg-cyan-500 transition ease-in-out hover:scale-105"
        >
          <ActiveBtnDotQuo />
          <p className="font-semibold">
            {locale === "mn" ? "Үнийн санал авах" : "Request for quotation"}
          </p>
        </button>
      </div>
      <div className="sm:w-1/3 sm:h-[300px] relative sm:block hidden overflow-hidden">
        <Image
          fill
          alt="img"
          src="/logo.png"
          sizes="33vw"
          style={{ objectFit: "contain" }}
          // placeholder="blur"
          // blurDataURL="/logo_blur.png"
        />
      </div>
    </div>
  );
};

export default QuotationBanner;
