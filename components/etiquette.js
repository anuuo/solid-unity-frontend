import React from "react";
import { useTranslation } from "next-i18next";

import { GiArcheryTarget, GiLightBulb, GiStairsGoal } from "react-icons/gi";
import { MiniTitle } from "@/lib/title";
import { mergeNames } from "../utils";
import { ContainerX } from "@/lib/layout";

// "sky-500"
// "teal-500"

const Card = ({ data = {}, id = 1, classname = "" }) => {
  return (
    <div
      className={mergeNames(
        "group flex rounded-xl shadow-lg overflow-hidden w-full",
        classname
      )}
    >
      <div className="w-full py-6 px-6 bg-slate-800 group-hover:scale-110 transition-all duration-150 border-b-0 border-sky-700 group-hover:border-teal-700 ease-in flex flex-col items-center">
        <div className="rounded-full bg-sky-700 shadow-lg w-fit p-2 group-hover:bg-teal-500">
          {id === 1 && <GiArcheryTarget size={35} color={"white"} />}
          {id === 2 && <GiStairsGoal size={35} color={"white"} />}
          {id === 3 && <GiLightBulb size={35} color={"white"} />}
        </div>
        <div className="group-hover:text-black py-3">
          <MiniTitle color="white">{data?.title}</MiniTitle>
        </div>
        {typeof data?.value === "string" ? (
          <p className="text-gray-300 font-light sm:text-base text-sm group-hover:text-white">
            {data?.value}
          </p>
        ) : (
          data?.value?.map((item, id) => (
            <p
              key={id}
              className="text-gray-300 font-light sm:text-base text-sm group-hover:text-white"
            >
              {item}
            </p>
          ))
        )}
      </div>
    </div>
  );
};

const Etiquette = () => {
  const { t } = useTranslation("ethic");
  return (
    <div className="w-full py-32 text-center bg-bgdark relative">
      <ContainerX>
        <p className="md:text-5xl text-3xl pb-4 text-slate-100 font-extralight ">
          {t("title")}
        </p>

        {/* <div className="grid grid-cols-3 gap-5" > */}
        <div className="flex sm:flex-row flex-col justify-evenly sm:gap-5 gap-8 mt-6">
          <Card
            id={1}
            data={t("goal", { returnObjects: true })}
            classname="w-full"
          />

          <Card
            id={3}
            data={t("value", { returnObjects: true })}
            // classname="w-full"
          />
          <Card
            id={2}
            data={t("principle", { returnObjects: true })}
            // classname="w-2/5"
          />
        </div>
      </ContainerX>
    </div>
  );
};

export const EtiquettePlain = () => {
  const { t } = useTranslation("ethic");
  return (
    <div>
      <div className="flex sm:flex-row flex-col justify-evenly sm:gap-5 gap-8 mt-6">
        <Card id={1} data={t("goal", { returnObjects: true })} />
        <Card id={2} data={t("principle", { returnObjects: true })} />
        <Card id={3} data={t("value", { returnObjects: true })} />
      </div>
    </div>
  );
};

export default Etiquette;
