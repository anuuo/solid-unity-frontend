import { SubTitleWhite } from "@/lib/title";
import { useForm } from "react-hook-form";
import React from "react";

import {
  IoMailOpenOutline,
  IoCallOutline,
  IoLocation,
  IoLocationSharp,
} from "react-icons/io5";
import { IoMdTime } from "react-icons/io";

import emailjs from "@emailjs/browser";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Spining from "@/lib/spining";
import { useTranslation } from "next-i18next";
import { default as FormLabel } from "@/lib/form/label";
import { GoLocation } from "react-icons/go";

const SectionInfo = ({ label, value, Icon }) => {
  return (
    <div className="flex flex-row items-center">
      <div className="w-1/6 sm:w-auto">
        <Icon />
      </div>
      <div className=" w-5/6 sm:w-auto">
        <p className=" text-left text-lg font-medium text-gray-300 ">
          {label}:{" "}
        </p>
        <p className="text-left text-lg text-gray-100 break-words">{value}</p>
      </div>
    </div>
  );
};

const Contact = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm();
  const { t } = useTranslation();

  const [loadingSend, setLoadingSend] = React.useState(false);
  const [submitBtnDisabled, setSubmitBtnDisabled] = React.useState(false);

  const onSubmit = async (data) => {
    const templateParams = data;
    setLoadingSend(true);
    setSubmitBtnDisabled(true);
    await emailjs
      .send(
        "service_ldj2esh",
        "template_gl8o1wd",
        templateParams,
        "gSuD5_a7MZhrv4alE"
      )
      .then(
        (response) => {
          reset();
          setLoadingSend(false);
          if (response?.status === 200) {
            toast.success("Таны и-мэйл амжилттай илгээгдлээ.", {
              position: "top-right",
              autoClose: 3000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
              theme: "dark",
            });
          }
          // console.log("SUCCESS!", response.status, response.text);
        },
        (err) => {
          setLoadingSend(false);
          // console.log("FAILED...", err);
          toast.error("Амжилтгүй", {
            position: "top-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "dark",
          });
        }
      );
    await setSubmitBtnDisabled(false);
  };

  return (
    <div>
      <ToastContainer />
      <SubTitleWhite>{t("contact:title")}</SubTitleWhite>
      <div className="flex sm:flex-row flex-col justify-between mt-10 lg:gap-10">
        <div className="flex flex-col sm:w-1/2 w-full gap-4">
          <SectionInfo
            label={t("contact:label1")}
            value={t("contact:value1")}
            Icon={() => (
              <IoMailOpenOutline
                size={40}
                color="bg-primary"
                className="text-sky-700 mr-2"
              />
            )}
          />
          <SectionInfo
            label={t("contact:label2")}
            value={t("contact:value2")}
            Icon={() => (
              <IoCallOutline
                size={40}
                color="bg-primary"
                className="text-sky-700 mr-2"
              />
            )}
          />

          <SectionInfo
            label={t("contact:label3")}
            value={t("contact:value3")}
            Icon={() => (
              <IoMdTime
                size={40}
                color="bg-primary"
                className="text-sky-700 mr-2"
              />
            )}
          />
          <SectionInfo
            label={t("contact:label4")}
            value={t("contact:value4")}
            Icon={() => (
              <GoLocation
                size={40}
                color="bg-primary"
                className="text-sky-700 mr-2"
              />
            )}
          />
          {/* <div className="mt-10 w-full hidden sm:block"> */}
          <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2674.173639903588!2d106.89195181554895!3d47.91367757470764!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5d96925607144283%3A0x72e09e64f1d190e3!2sRokmon!5e0!3m2!1sen!2smn!4v1661386339006!5m2!1sen!2smn"
            //   width="600"
            // height="450"
            loading="lazy"
            className="mt-4 w-full block md:pr-8 pl-2 min-h-[200px] rounded"
            referrerPolicy="no-referrer-when-downgrade"
          />
        </div>
        <div className="flex flex-col sm:w-1/2 w-full mt-10 sm:mt-0 justify-start sm:px-10 ">
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="flex flex-col gap-6"
          >
            <div className="w-full">
              <FormLabel req={true} text={t("mail:label1")} />
              <input
                type="text"
                placeholder={t("mail:label1")}
                {...register("name", { required: true })}
                className="bg-slate-700 text-white outline-none px-4 py-2 rounded w-full"
              />
            </div>
            <div className="w-full">
              <FormLabel req={true} text={t("mail:label2")} />
              <input
                type="email"
                placeholder={t("mail:label2")}
                {...register("email", {
                  required: true,
                  pattern: /^\S+@\S+$/i,
                })}
                className="bg-slate-700 text-white outline-none px-4 py-2 rounded w-full"
              />
            </div>
            <div className="w-full">
              <FormLabel req={true} text={t("mail:label3")} />
              <input
                type="text"
                placeholder={t("mail:label3")}
                {...register("title", { required: true })}
                className="bg-slate-700 text-white outline-none px-4 py-2 rounded w-full"
              />
            </div>
            <div className="w-full">
              <FormLabel req={true} text={t("mail:label4")} />
              <textarea
                placeholder={t("mail:label4")}
                {...register("message", { required: true })}
                className="bg-slate-700 text-white outline-none px-4 py-2 rounded w-full"
              />
            </div>

            <div>
              <button
                type="submit"
                disabled={submitBtnDisabled}
                className="bg-sky-600 text-white px-4 py-2 rounded-lg hover:bg-teal-600  transition-all ease-in-out flex flex-row items-center justify-evenly hover:scale-105"
              >
                {loadingSend && <Spining />}
                {t("mail:btn")}
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Contact;
