import React from "react";

/* eslint-disable @next/next/no-img-element */

import Image from "next/image";
import Link from "next/link";
import style from "@/styles/background.module.css";
import { ActiveBtnDot } from "@/lib/btn";
import { useBreakpoints } from "../hooks";
import { useRouter } from "next/router";
import { mergeNames } from "../utils";

const TextName = (props) => {
  return (
    <p className="lg:text-2xl text-lg capitalize font-medium tracking-wide pt-6 text-slate-300">
      {props.children}
    </p>
  );
};

const NewAboutusCard = ({
  id = 0,
  btn = "",
  name = "",
  img,
  job = "",
  age = "",
  locale = "mn",
  lastname = "",
  slug = "",
}) => {
  const router = useRouter();

  const handleClick = (link) => {
    router.push(link);
  };
  return (
    <div
      className={mergeNames(
        "relative rounded-lg md:pb-10 pb-4"
        // id == 12 && "md:col-start-2",
        // id == 13 && "md:col-start-3"
      )}
    >
      <button
        className="w-full relative flex justify-center items-center"
        onClick={() => handleClick(`/aboutus/${id}`)}
      >
        <Image
          alt={name}
          width={220}
          height={220}
          placeholder="blur"
          src={`/team${img.high}`}
          blurDataURL={`/team${img.low}`}
          className="rounded-full aspect-square object-cover object-top hover:scale-105 transition-all ease-in-out"
        />
      </button>

      <div className="flex flex-col items-center">
        <TextName>
          {locale === "mn"
            ? `${lastname.charAt(0)}. ${name}`
            : `${name} .${lastname.charAt(0)}`}
        </TextName>

        <p className="md:text-xl tracking-wide text-base text-sky-500">{job}</p>

        {/* <Link href={`/aboutus/${id}`} className="bg-sky-500">
          {locale === "mn" ? "Илүү" : "More"}
        </Link>  */}
        <div className="w-fit relative">
          <div className="rounded px-2 py-1 mt-2 border border-sky-800 group-hover:bg-sky-500 transition-all ease-in-out">
            {id === 0 && <ActiveBtnDot />}

            <Link
              href={`/aboutus/${id}`}
              className="text-sm text-sky-100  group-hover:text-white"
            >
              {locale === "mn" ? "Дэлгэрэнгүй" : "Show more"}
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewAboutusCard;
