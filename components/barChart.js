import { useBreakpoints, useDimensions } from "@/hooks/index";
import { numberFormatter } from "@/utils/index";
import { useTranslation } from "next-i18next";
import React from "react";
import {
  ResponsiveContainer,
  ComposedChart,
  // BarChart as ReBarchart,
  Bar,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Line,
  Legend,
} from "recharts";
import { CustomTooltipBarChart } from "./aboutus/toolTip";

const data = [
  { name: "2019", revenue: 3 },
  { name: "2020", revenue: 17 },
  { name: "2021", revenue: 39 },
  { name: "2022", revenue: 39 },
];

// const resolveSize = (point, screenWidth) => {
//   switch (point) {
//     case "default":
//     case "xs":
//     case "sm":
//       return { width: screenWidth - 100, height: 200 };
//     case "md":
//       return { width: screenWidth - 300, height: 200 };
//     case "lg":
//       return { width: 400, height: 300 };
//     case "xl":
//     case "xxl":
//     case "3xl":
//       return { width: 500, height: 300 };
//     default:
//       return { width: screenWidth - 100, height: 300 };
//   }
// };

const resolveHeight = (point) => {
  switch (point) {
    case "default":
    case "xs":
    case "sm":
      return 250;
    case "md":
    case "lg":
    case "xl":
    case "xxl":
    case "3xl":
      return 300;
    default:
      return 250;
  }
};

const BarChart = ({ locale }) => {
  const { t } = useTranslation("service");
  const point = useBreakpoints();
  const height = resolveHeight(point) || 250;

  // console.log("heihth", height);
  const name = locale === "mn" ? "харилцагчийн тоо" : "the number of customers";
  return (
    <div className="flex flex-col justify-between items-center gap-10 ">
      <div style={{ width: "95%", height: height }}>
        <ResponsiveContainer>
          <ComposedChart
            data={data}
            margin={{
              top: 5,
              right: 0,
              left: -30,
              bottom: 5,
            }}
          >
            <CartesianGrid stroke="rgba(255,255,255,0.3)" />
            <Bar
              domain={[0, 45]}
              name="харилцагчийн тоо"
              dataKey="revenue"
              fill="rgba(24, 144, 255, 0.3)"
            />
            <Line
              name="харилцагчийн тоо"
              type="monotone"
              dataKey="revenue"
              stroke="rgba(24, 144, 255, 1)"
            />

            <XAxis dataKey="name" tick={{ fontSize: 11, fill: "white" }} />
            <YAxis
              type="number"
              dataKey={"revenue"}
              domain={[0, 45]}
              tickFormatter={numberFormatter}
              tick={{ fontSize: 11, fill: "white" }}
            />
            <Tooltip content={<CustomTooltipBarChart />} />
            <Legend />
          </ComposedChart>
        </ResponsiveContainer>
      </div>
      <div className="text-left flex sm:flex-row flex-col justify-between items-center sm:gap-10 gap-5">
        {t("customerNumber", { returnObjects: true })?.para?.map(
          (item, key) => {
            return (
              <p
                key={key}
                className="text-slate-50 font-extralight  text-lg text-justify indent-8"
              >
                {item}
              </p>
            );
          }
        )}
      </div>
    </div>
  );
};

export default BarChart;
