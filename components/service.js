import React, { useEffect, useState } from "react";
import { IoIosFingerPrint, IoIosPaperPlane } from "react-icons/io";
import { IoCalculator } from "react-icons/io5";
import { SubTitleLg } from "@/lib/title";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";
import { ContainerX } from "@/lib/layout";

const Card = ({ img: src, title, handleClick, btnName }) => (
  <button className="group h-1/2 overflow-hidden" onClick={handleClick}>
    <div className="rounded-2xl overflow-hidden shadow-lg relative  transition-transform ease-in scale-[0.85] group-hover:scale-90 h-full w-full">
      <img src={src} alt={"img"} className="h-full w-full object-cover" />
      <div className=" group-hover:bg-gray-900/50 absolute top-0 right-0 left-0 bottom-0 flex flex-col justify-center items-center text-center bg-zinc-900/70">
        <p className="md:text-3xl text-xl font-light text-center text-blue-100/90 group-hover:text-white px-4">
          {title}
        </p>
        {/* <div className="w-min bg-sky-500 px-3 py-1 rounded-full mt-2 ">
          <p className="text-white font-semibold ">{btnName}</p>
        </div> */}
      </div>
    </div>
  </button>
);

const Service = ({ locale }) => {
  const router = useRouter();
  const [btnName, setBtnName] = useState(() =>
    locale === "mn" ? "Дэлгэрэнгүй" : "More"
  );
  useEffect(() => {
    if (locale === "mn") {
      setBtnName("Дэлгэрэнгүй");
    } else {
      setBtnName("More");
    }
  }, [locale]);

  const { t } = useTranslation("service");
  const handleClick = () => {
    router.push("/service");
  };

  return (
    <div className="w-full min-h-[94vh] flex items-center bg-bglight text-gray-300 text-base relative">
      <ContainerX>
        <div className="flex-1 flex flex-row h-full ">
          <div className="sm:flex hidden flex-col w-1/3 gap-2">
            <Card
              btnName={btnName}
              img={t("audit", { returnObjects: true })?.img}
              title={t("audit", { returnObjects: true })?.title}
              handleClick={handleClick}
            />
            <Card
              btnName={btnName}
              img={t("consult", { returnObjects: true })?.img}
              title={t("consult", { returnObjects: true })?.title}
              handleClick={handleClick}
            />
            {/* <Card
            btnName={btnName}
            img={t("valuation", { returnObjects: true })?.img}
            title={t("valuation", { returnObjects: true })?.title}
            handleClick={handleClick}
          /> */}
          </div>
          <div className="flex-1 flex flex-col w-3/4 md:pl-20 justify-evenly">
            <div>
              <div className="text-center sm:text-left">
                <SubTitleLg color="white">{t("title")}</SubTitleLg>
              </div>
              {/* <p>{t("desc")}</p> */}
            </div>
            <div className="flex flex-col gap-4 justify-center">
              <SectionText
                btnName={btnName}
                handleClick={handleClick}
                title={t("audit", { returnObjects: true })?.title}
                text={t("audit", { returnObjects: true })?.desc}
                Icon={() => (
                  <IoIosFingerPrint size={20} className="text-primary" />
                )}
              />
              <SectionText
                btnName={btnName}
                handleClick={handleClick}
                title={t("consult", { returnObjects: true })?.title}
                text={t("consult", { returnObjects: true })?.desc}
                Icon={() => (
                  <IoIosPaperPlane size={20} className="text-primary" />
                )}
              />
              {/* <SectionText
                btnName={btnName}
                handleClick={handleClick}
                title={t("valuation", { returnObjects: true })?.title}
                text={t("valuation", { returnObjects: true })?.desc}
                Icon={() => <IoCalculator size={20} className="text-primary" />}
              /> */}
              {/* {t("list", { returnObjects: true })?.map(
              ({ id = Math.random(), title = "title", img, body }) => {
                return (
                  <SectionText
                    key={id}
                    title={title}
                    text={body}
                    Icon={() => {
                      switch (id) {
                        case 0:
                          return (
                            <IoIosFingerPrint
                              size={20}
                              className="text-primary"
                            />
                          );
                        case 1:
                          return (
                            <IoCalculator size={20} className="text-primary" />
                          );
                        case 2:
                        default:
                          return (
                            <IoIosPaperPlane
                              size={20}
                              className="text-primary"
                            />
                          );
                      }
                    }}
                  />
                );
              }
            )} */}
            </div>
          </div>
        </div>
      </ContainerX>
    </div>
  );
};

export const SectionText = ({ Icon, title, text, handleClick, btnName }) => {
  return (
    <div className="flex flex-row items-center sm:space-y-4 space-y-2">
      <div className="rounded-full p-2 bg-slate-700/60 mr-4">
        <Icon />
      </div>
      <div>
        <p className="sm:text-xl text-lg capitalize  pb-1 text-sky-500 font-semibold">
          {title}
        </p>
        <p className="text-gray-300 sm:text-lg text-base">
          {text}
          {/* <button
            onClick={handleClick}
            className="text-sky-500 font-semibold pl-2"
          >
            {btnName}
          </button> */}
        </p>
        <div className="sm:hidden block pt-3">
          <button
            onClick={handleClick}
            className="border rounded-md border-sky-500 text-sky-500 font-semibold py-1 px-2 sm:hidden block"
          >
            {btnName}
          </button>
        </div>
      </div>
    </div>
  );
};

export default Service;
