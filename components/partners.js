import partnersdata from "@/data/partners";
import { useBreakpoints } from "@/hooks/index";
import { SubTitleLg, SubTitleWhite } from "@/lib/title";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import React from "react";

const Partners = () => {
  const point = useBreakpoints();
  const { t } = useTranslation("partner");
  return (
    <div className="w-full lg:px-40 px-10 py-24 text-gray-500 text-base text-center relative bg-bgdark">
      <div className="pb-8">
        <SubTitleWhite color="gray-50" align="left">
          {t("title")}
        </SubTitleWhite>
      </div>
      <div className="grid md:grid-cols-5 sm:grid-cols-4 grid-cols-3 xs:gap-x-20  lg:gap-x-20 gap-x-2 gap-y-4">
        {partnersdata?.map(({ id, img, name }) => {
          return (
            <div key={id} className="lg:px-4 py-0">
              <Image
                src={img}
                alt={name}
                objectFit="contain"
                layout="responsive"
                width={point === "sm" || "xs" ? 200 : 200}
                height={point === "sm" || "xs" ? 100 : 70}
              />
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Partners;
