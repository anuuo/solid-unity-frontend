import React from "react";
import { SubTitleWhite } from "@/lib/title";
import { useTranslation } from "next-i18next";
import BarChart from "@/components/barChart";

const NumberCustomerChart = ({ locale }) => {
  const { t } = useTranslation("service");
  return (
    <div className="">
      <div className="py-6">
        <SubTitleWhite>
          {t("customerNumber", { returnObjects: true })?.title}
        </SubTitleWhite>
      </div>
      <BarChart locale={locale} />
    </div>
  );
};

export default NumberCustomerChart;
