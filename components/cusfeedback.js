/* eslint-disable @next/next/no-img-element */
import React from "react";
import { IoIosQuote, IoIosStar } from "react-icons/io";

import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination, A11y } from "swiper";

import styles from "@/styles/background.module.css";
// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import { useTranslation } from "next-i18next";

const Card = ({ text = "", name = "", star = 5 }) => {
  return (
    <div className="h-[50vh] w-[70vw] bg-slate-600 rounded-3xl md:p-4  p-2 flex flex-col justify-center items-center text-gray-500 text-center overflow-hidden">
      <IoIosQuote className="text-gray-300 sm:text-[200px] text-[100px]" />

      <p className="sm:px-10 px-2 text-lg text-gray-100">{text}</p>
      <div className="py-6 flex flex-col items-center">
        <p className="sm:text-2xl text-gray-50 text-2xl capitalize pb-2">
          {name}
        </p>
        <div className="flex flex-row">
          {Array(star)
            .fill()
            .map((id) => (
              <IoIosStar
                key={Math.random()}
                size={20}
                className="text-amber-300"
              />
            ))}
        </div>
      </div>
    </div>
  );
};

export default function CustomerFeedback() {
  const { t } = useTranslation("feedback");
  return (
    <div className="w-full relative overflow-hidden bg-center bg-cover h-[80vh]">
      <div className={styles.feedbackBgImageOverlay}>
        <Swiper
          navigation
          // spaceBetween={50}
          slidesPerView={"auto"}
          centeredSlides={true}
          pagination={{ clickable: true }}
          scrollbar={{ draggable: true }}
          modules={[Navigation, Pagination, A11y]}
          grabCursor={true}
          loop={true}
          autoplay={{
            delay: 2500,
            disableOnInteraction: false,
          }}
          // onSwiper={(swiper) => console.debug(swiper)}
          // onSlideChange={() => console.debug("slide change")}
        >
          <SwiperSlide key={t("user1")}>
            <div
              className="flex justify-center items-center w-full h-[80vh]"
              key={t("user1")}
            >
              <Card name={t("user1")} text={t("user1_msg")} />
            </div>
          </SwiperSlide>
          <SwiperSlide key={t("user2")}>
            <div
              className="flex justify-center items-center w-full h-[80vh]"
              key={t("user2")}
            >
              <Card name={t("user2")} text={t("user2_msg")} />
            </div>
          </SwiperSlide>
          {/* {feedbackdata?.map(({ name = "", text = "" }) => {
            return (
              <SwiperSlide key={name}>
                <div
                  className="flex justify-center items-center w-full h-[80vh]"
                  key={name}
                >
                  <Card
                    name={feedbackdata[0]?.name ?? ""}
                    text={feedbackdata[0]?.text ?? ""}
                  />
                </div>
              </SwiperSlide>
            );
          })} */}
        </Swiper>
      </div>
    </div>
  );
}
