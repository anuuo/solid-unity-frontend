import React from "react";
import { Title, ListView, DownloadButton } from "@/components/hr/micro";
import { BiTime, BiUserCheck } from "react-icons/bi";

const JobIntroduction = ({ title, hour, type, mission }) => {
  return (
    <React.Fragment>
      <div className="flex flex-col gap-2 pb-4">
        <p className="text-white md:text-4xl sm:text-3xl text-2xl font-light uppercase tracking-wider">
          {title}
        </p>
        <div className="h-1 w-20 bg-sky-500 rounded-lg" />
      </div>
      <div className="flex flex-row items-center gap-1">
        <BiTime size={20} className="text-sky-500" />
        <p className="text-lg">{hour}</p>
      </div>
      <div className="flex flex-row items-center gap-1">
        <BiUserCheck size={20} className="text-sky-500" />
        <p className="text-lg">{type}</p>
      </div>
      <div className="py-4">
        <Title>{mission?.title}:</Title>
        <div className="sm:pl-8 pl-0">
          <p className="sm:indent-0 text-lg">{mission?.value}</p>
        </div>
      </div>
    </React.Fragment>
  );
};
export default JobIntroduction;
