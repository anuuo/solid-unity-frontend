import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import { IoMdDownload } from "react-icons/io";

export const Title = (props) => {
  return (
    <p className="font-semibold sm:text-2xl text-xl text-sky-500 pb-1">
      {props?.children}
    </p>
  );
};

export const ListView = ({ item }) => {
  return (
    <ul className="list-disc">
      <li className="py-1 sm:py-[2px] sm:text-lg text-base">{item}</li>
    </ul>
  );
};

export const DownloadButton = ({
  locale = "mn",
  handleDownload = () => {},
}) => {
  const router = useRouter();
  // eslint-disable-next-line @next/next/no-html-link-for-pages
  // return <a href="/api/downloadFile">Download PDF</a>;

  return (
    <div className="pt-6 md:pl-8 pl-0">
      {/* <a href="/api/downloadFile"> */}
      <button
        className="flex flex-row gap-2 items-center bg-sky-500 rounded-md px-4 py-2 hover:bg-teal-500 transition-all ease-in-out hover:scale-110"
        onClick={() => router.push("/anket.doc")}
      >
        {/* AiOutlineDownload */}
        <p className="text-bgdark font-semibold">
          {locale === "mn" ? "Анкет татаж авах" : "Download Resume"}
        </p>
        <IoMdDownload className="text-bgdark font-semibold" size={20} />
      </button>
      {/* </a> */}
    </div>
  );
};
