import numberFormatter from "@/utils/numberFormatter";
import React from "react";

export const CustomTooltipPieChart = ({ active, payload, label }) => {
  if (active) {
    return (
      <div
        className="custom-tooltip"
        style={{
          backgroundColor: "#0E2036",
          padding: "5px",
          borderRadius: "10px",
        }}
      >
        <label>{`${payload[0].name} : ${payload[0].value}%`}</label>
      </div>
    );
  }
  return null;
};

export const CustomTooltipBarChart = ({ active, payload, label }) => {
  if (active) {
    const money = numberFormatter(payload[0].value);
    return (
      <div
        className="custom-tooltip"
        style={{
          padding: "5px",
          borderRadius: "10px",
          borderColor: "black",
          backgroundColor: "black",
        }}
      >
        <label>{`харилцагчийн тоо : ${money}`}</label>
      </div>
    );
  }
  return null;
};
