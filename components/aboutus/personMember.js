import React from "react";
import { PersonTitle } from "./personMicro";

const PersonMember = ({ membership }) => {
  if (typeof membership === "object")
    return (
      <div className="py-4 flex flex-col gap-1">
        <PersonTitle>{membership?.title}</PersonTitle>
        <div>
          {membership?.value?.map((item, key) => {
            return (
              <ul
                key={key}
                className="list-disc text-base py-[3px] flex flex-row pl-8"
              >
                <li className="sm:text-xl text-lg">{item}</li>
                {/* <p className="pl-2"></p> */}
              </ul>
            );
          })}
        </div>
      </div>
    );
  return <></>;
};

export default PersonMember;
