import { SubTitleWhite } from "@/lib/title";
import { useTranslation } from "next-i18next";
import { clearPreviewData } from "next/dist/server/api-utils";
import React from "react";
import {
  ResponsiveContainer,
  PieChart as RePiechart,
  Pie,
  Cell,
  Label,
  Tooltip,
  Legend,
} from "recharts";
import { CustomTooltipPieChart } from "./toolTip";
import VerticalChart from "./verticalChart";

const COLORS = ["rgba(24, 144, 255, 0.9)", "#0891b2", "#0e7490", "#0ea5e9"];
const pieData = [
  {
    name: "Магистр",
    value: 6.25,
  },
  {
    name: "Бакалавр",
    value: 68.75,
  },
  {
    name: "Дэд профессор",
    value: 6.25,
  },
  {
    name: "Профессор",
    value: 18.75,
  },
];

const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
  cx,
  cy,
  midAngle,
  innerRadius,
  outerRadius,
  percent,
  index,
}) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text
      x={x}
      y={y}
      fill="white"
      textAnchor="middle"
      dominantBaseline="central"
      fontSize={10}
    >
      {`${(percent * 100).toFixed(0)}%`}
    </text>
  );
};

export default function PieChart() {
  const { t } = useTranslation("aboutus");
  const data = t("education", { returnObjects: true }) || pieData;
  return (
    <div>
      <SubTitleWhite>{t("subTitle4")}</SubTitleWhite>
      <div className="flex md:flex-row flex-col justify-evenly items-center md:gap-10 gap-8 text-lg">
        <div className="h-[300px] md:w-1/2 w-full">
          <ResponsiveContainer>
            <RePiechart>
              <Pie
                data={data}
                color="#000000"
                dataKey="value"
                nameKey="name"
                cx="50%"
                cy="50%"
                stroke="#0E2036"
                outerRadius={120}
                fill="#8884d8"
                labelLine={false}
                label={renderCustomizedLabel}
              >
                {data.map((entry, index) => (
                  <Cell
                    key={`cell-${index}`}
                    fill={COLORS[index % COLORS.length]}
                  >
                    <p className="text-lg text-black">hello</p>
                  </Cell>
                ))}
              </Pie>
              <Tooltip content={<CustomTooltipPieChart />} />

              <Legend />
            </RePiechart>
          </ResponsiveContainer>
        </div>
        <div className="md:w-1/2 w-full">
          <VerticalChart />
        </div>
      </div>
    </div>
  );
}
