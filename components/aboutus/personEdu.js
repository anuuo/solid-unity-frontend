import React from "react";
import { PersonTitle, PersonText, InactiveDot } from "./personMicro";

const TimeText = (props) => {
  return (
    <time className=" text-base font-normal leading-none text-gray-400 dark:text-gray-500">
      {props.children}
    </time>
  );
};

{
  /* <h3 className="text-lg font-semibold text-gray-900 dark:text-white">
            Marketing UI design in Figma
          </h3> */
}
const PersonEdu = ({ education = {} }) => {
  return (
    <div className="py-4 flex flex-col gap-1">
      <div className="mb-2">
        <PersonTitle>{education?.title}</PersonTitle>
      </div>
      <div className="pl-8">
        <ol className="relative border-l border-gray-200 dark:border-gray-700">
          {education?.value?.map(({ year, text }, key) => {
            return (
              <li className="mb-6 ml-4" key={key}>
                <InactiveDot />
                <TimeText>{year}</TimeText>
                <PersonText>{text}</PersonText>
              </li>
            );
          })}
        </ol>
      </div>
    </div>
  );
};

export default PersonEdu;
