import { useTranslation } from "next-i18next";
import React from "react";

const data = [
  {
    name: "Мэргэшсэн нягтлан бодогч",
    percent: "57%",
    number: 57,
  },
  {
    name: "Аудитор",
    percent: "46%",
    number: 46,
  },
  {
    name: "Хөрөнгийн үнэлгээчин",
    percent: "23%",
    number: 23,
  },
];

const VerticalChart = () => {
  const { t } = useTranslation("aboutus");
  const graphData = t("rank", { returnObjects: true }) || data;
  return (
    <div className="flex flex-col md:gap-6 gap-4">
      {graphData?.map(({ name, percent, number }) => {
        const reverse = 100 - number;
        return (
          <div key={name}>
            <p className="text-left pb-2">
              {name}: {percent}
            </p>
            <div className="w-full flex flex-row rounded-md overflow-hidden">
              {/* <div className={`h-4 w-${percent} bg-cyan-700/70`} /> */}
              <div
                style={{
                  width: percent,
                  backgroundColor: "rgb(14 116 144 /1)",
                }}
                className="p-1"
              >
                <p className="text-lg">{percent}</p>
              </div>
              <div
                style={{
                  width: `${reverse}%`,
                  backgroundColor: "rgb(14 116 144 / 0.1)",
                }}
                className="p-1"
              >
                <p className="text-lg">{reverse}%</p>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default VerticalChart;
