import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";

const Text = (props) => {
  return (
    <a className="ml-1 text-base font-medium text-gray-700 hover:text-gray-900 md:ml-2 dark:text-gray-400 dark:hover:text-white cursor-pointer">
      {props.children}
    </a>
  );
};
const Arrow = () => {
  return (
    <svg
      className="w-6 h-6 text-gray-400"
      fill="currentColor"
      viewBox="0 0 20 20"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
        clipRule="evenodd"
      ></path>
    </svg>
  );
};

const PersonBreadCrumb = ({ locale = "mn", name = "", id }) => {
  const router = useRouter();
  const handleClick = (id) => {
    // console.log("CLICKED HERE");
    router.push(`/aboutus/${id}`);
  };
  const handleBack = () => {
    router.push(`/aboutus/`);
  };

  return (
    <nav className="flex" aria-label="Breadcrumb">
      <ol className="inline-flex items-center space-x-1 md:space-x-3">
        <li className="inline-flex items-center">
          <button onClick={handleBack}>
            <Text>{locale === "mn" ? "Биднийн тухай" : "About us"}</Text>
          </button>
        </li>
        <li aria-current="page">
          <div className="flex items-center">
            <Arrow />
            <button onClick={() => handleClick(id)}>
              <Text>{name}</Text>
            </button>
          </div>
        </li>
      </ol>
    </nav>
  );
};

export default PersonBreadCrumb;
