import React from "react";

const PersonTitle = (props) => {
  // return (
  //   <h2 className="text-2xl font-bold md:text-4xl">{props?.children ?? ""}</h2>
  // );
  return (
    <p className="text-sky-500 sm:text-3xl text-2xl font-bold">{props?.children ?? ""}: </p>
  );
};
const PersonText = (props) => {
  return (
    <p className="sm:text-xl text-lg text-justify font-normal text-gray-300/90">{props.children}</p>
  );
};
const InactiveDot = ({ ab = true }) => {
  if (ab)
    return (
      <div className="absolute w-3 h-3 bg-sky-400 rounded-full mt-1.5 -left-1.5 border border-white dark:border-gray-900 "></div>
    );
  return (
    <div className="w-3 h-3 bg-transparent rounded-full border border-gray-700 "></div>
  );
};
const ActiveDot = () => {
  return (
    <div className="w-3 h-3 bg-gray-700 rounded-full border border-white dark:border-gray-900 "></div>
  );
};

export { PersonTitle, PersonText, ActiveDot, InactiveDot };
