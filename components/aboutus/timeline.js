import { SubTitleWhite } from "@/lib/title";
import { useTranslation } from "next-i18next";
import React, { useState } from "react";
import VisibilitySensor from "react-visibility-sensor";

const Title = (props) => (
  <h4 className="md:mb-3 mb-1 md:font-semibold uppercase text-lg md:text-2xl">
    {props.children}
  </h4>
);
const DateText = (props) => (
  <p className="md:mb-3 mb-2 md:text-base text-base text-sky-700">{props.children}</p>
);
const Text = (props) => (
  <p className="font-light leading-snug text-gray-100 text-opacity-100 md:text-base text-base">
    {props.children}
  </p>
);

const RightTimeLine = ({ date = "", title = "meaning", text = "" }) => {
  return (
    <div className="mb-4 flex justify-between items-center w-full right-timeline">
      <div className="order-1 w-5/12"></div>
      <div className="order-1  w-5/12 px-1 py-4 text-left">
        <DateText>{date}</DateText>
        <Title>{title}</Title>
        {/* <h4 className="mb-3 font-bold text-sky-700 text-lg md:text-2xl">
          {date}
        </h4> */}
        <Text>{text}</Text>
      </div>
    </div>
  );
};

const LeftTimeline = ({ date = "", title = "meaning", text = "" }) => {
  return (
    <div className="mb-4 flex justify-between flex-row-reverse items-center w-full left-timeline">
      <div className="order-1 w-5/12"></div>
      <div className="order-1 w-5/12 px-1 py-4 text-right">
        <DateText>{date}</DateText>
        <Title>{title}</Title>
        {/* <h4 className="mb-3 font-bold text-sky-700 text-lg md:text-2xl">
          {date}
        </h4> */}
        <Text>{text}</Text>
      </div>
    </div>
  );
};

const Timeline = () => {
  const { t } = useTranslation("aboutus");
  const ref = React.useRef(null);

  function onChange(isVisible) {
    if (isVisible && typeof window !== "undefined") {
      const el2 = ref.current;
      el2.contentDocument.children[0].classList = "animated";
    }
  }

  return (
    <div>
      <div className="text-slate-300 ">
        <div className="container mx-auto flex flex-col items-start md:flex-row">
          <div className="flex flex-col justify-center items-center w-full sticky md:top-36 lg:w-1/3 mt-2 md:mt-12">
            <p className="md:text-5xl text-3xl text-slate-50 font-extralight pb-6">
              {t("subTitle2")}
            </p>
            <VisibilitySensor onChange={onChange}>
              <object
                id="svgObject"
                type="image/svg+xml"
                data="/img.svg"
                ref={ref}
              ></object>
            </VisibilitySensor>
          </div>

          <div className="ml-0 md:ml-12 lg:w-2/3 w-full sticky">
            <div className="container mx-auto w-full h-full">
              {/* <p className="ml-2 text-sky-700 uppercase tracking-loose text-base">
                2019 - 2022
              </p> */}
              <div className="relative wrap overflow-hidden h-full">
                <div className="border-2 absolute h-full right-1/2 border-sky-500 " />
                <div className="border-2 absolute h-full left-1/2 border-sky-500 " />
                {t("history", { returnObjects: true })?.map(
                  ({ date, text, title }, id) => {
                    if (id % 2 == 0)
                      return (
                        <LeftTimeline
                          key={Math.random()}
                          date={date}
                          text={text}
                          title={title}
                        />
                      );
                    else
                      return (
                        <RightTimeLine
                          key={Math.random()}
                          date={date}
                          title={title}
                          text={text}
                        />
                      );
                  }
                )}
              </div>

              {/* <img
                className="mx-auto -mt-36 md:-mt-36"
                alt="img"
                src={imgsvg}
                // src="https://user-images.githubusercontent.com/54521023/116968861-ef21a000-acd2-11eb-95ac-a34b5b490265.png"
              /> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Timeline;
