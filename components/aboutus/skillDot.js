import React from "react";
import { ActiveDot, InactiveDot } from "./personMicro";

const SkillDot = ({ value, dif }) => {
  return (
    <React.Fragment>
      {Array(value)
        .fill()
        .map(() => {
          return <ActiveDot key={Math.random()} />;
        })}
      {dif !== 0 &&
        Array(dif)
          .fill()
          .map(() => {
            return <InactiveDot key={Math.random()} ab={false} />;
          })}
    </React.Fragment>
  );
};

export default SkillDot;
