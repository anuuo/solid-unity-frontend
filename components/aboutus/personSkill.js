import React from "react";
import { PersonText, PersonTitle } from "./personMicro";
import SkillDot from "./skillDot";

const PersonSkill = ({ skill }) => {
  return (
    <div className="flex flex-col gap-3">
      <PersonTitle>{skill?.title}</PersonTitle>
      <div className="flex flex-col gap-2">
        {skill?.value?.map(({ title, value }, key) => {
          const dif = 4 - value;
          return (
            <div
              key={key}
              className="flex flex-row items-center gap-1 md:pl-0 pl-4"
            >
              <div className="md:w-auto w-1/4 flex flex-row items-center gap-1">
                <SkillDot value={value} dif={dif} />
              </div>
              <div className="md:w-auto w-3/4 md:pl-4">
                <PersonText>{title}</PersonText>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default PersonSkill;
