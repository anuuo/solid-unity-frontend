import React from "react";
import { PersonTitle } from "./personMicro";

const Icon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 512 512"
      className="w-4 h-4 fill-current text-sky-400"
    >
      <path d="M426.072,86.928A238.75,238.75,0,0,0,88.428,424.572,238.75,238.75,0,0,0,426.072,86.928ZM257.25,462.5c-114,0-206.75-92.748-206.75-206.75S143.248,49,257.25,49,464,141.748,464,255.75,371.252,462.5,257.25,462.5Z"></path>
      <polygon points="221.27 305.808 147.857 232.396 125.23 255.023 221.27 351.063 388.77 183.564 366.142 160.937 221.27 305.808"></polygon>
    </svg>
  );
};

const PersonJobExp = ({ experience }) => {
  // console.log("experience?.value", typeof experience?.value);
  return (
    <section>
      <PersonTitle>{experience?.title}</PersonTitle>
      {experience?.value && typeof experience?.value === "object" && (
        <div className="grid md:grid-cols-2 grid-cols-1 md:gap-6 gap-4 py-6 md:pl-8 pl-4">
          {experience?.value?.map(({ title, place, year }, key) => {
            return (
              <div key={key} className="space-y-8">
                <div>
                  <ul className="space-y-4">
                    <li className="space-y-1">
                      <div className="flex items-center space-x-2">
                        <Icon />
                        <p className="text-lg">{title}</p>
                      </div>
                      <p className="ml-7 dark:text-gray-400  text-base font-normal leading-none ">
                        {place} ({year})
                      </p>
                    </li>
                  </ul>
                </div>
              </div>
            );
          })}
        </div>
      )}
      {experience?.value && typeof experience?.value === "string" && (
        <div className="py-6 md:pl-8 pl-4">
          <p className="text-xl">{experience?.value}</p>
        </div>
      )}
    </section>
  );
};

export default PersonJobExp;
