import React, { useState } from "react";

import emailjs from "@emailjs/browser";
import { useForm } from "react-hook-form";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import Spining from "@/lib/spining";
import {
  FormWrapper,
  inputErrorStyle,
  inputStyle,
  SubmitButton,
} from "@/components/quotation/shared";

const ShortForm = ({ btnName, labels }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm();

  const [loadingSend, setLoadingSend] = React.useState(false);
  const [submitBtnDisabled, setSubmitBtnDisabled] = React.useState(false);

  const onSubmit = async (data) => {
    const templateParams = data;
    setLoadingSend(true);
    setSubmitBtnDisabled(true);
    await emailjs
      .send(
        "service_y8mqtl3",
        "template_2krfpg2",
        templateParams,
        "gSuD5_a7MZhrv4alE"
      )
      .then(
        (response) => {
          reset();
          setLoadingSend(false);
          if (response?.status === 200) {
            toast.success("Таны и-мэйл амжилттай илгээгдлээ.", {
              position: "top-right",
              autoClose: 3000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
              theme: "dark",
            });
          }
          // console.log("SUCCESS!", response.status, response.text);
        },
        (err) => {
          setLoadingSend(false);
          // console.log("FAILED...", err);
          toast.error("Амжилтгүй", {
            position: "top-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "dark",
          });
        }
      );
    await setSubmitBtnDisabled(false);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="grid md:grid-cols-3 sm:grid-cols-2 md:gap-x-10 sm:gap-x-5 gap-y-3">
        {/* Firstname */}
        <FormWrapper
          label={labels.name} // translation comes here
          errorMsg={errors?.firstname}
        >
          <input
            className={errors?.firstname ? inputErrorStyle : inputStyle}
            name={labels.name}
            placeholder={labels.name} // translation comes here
            {...register("firstname", {
              required: { value: true, message: "Заавал бөглөнө үү" },
            })}
          />
        </FormWrapper>

        {/* lastname */}
        <FormWrapper
          label={labels.lastname} // translation comes here
          errorMsg={errors?.lastname}
        >
          <input
            className={errors?.lastname ? inputErrorStyle : inputStyle}
            name={labels.lastname}
            placeholder={labels.name} // translation comes here
            {...register("lastname", {
              required: { value: true, message: "Заавал бөглөнө үү" },
            })}
          />
        </FormWrapper>

        {/* mail */}
        <FormWrapper label={labels.mail} errorMsg={errors?.mail}>
          <input
            className={errors?.mail ? inputErrorStyle : inputStyle}
            name="mail"
            placeholder={labels.mail}
            {...register("mail", {
              required: { value: true, message: "Заавал бөглөнө үү" },
              pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                message: "Зөв и-мэйл оруулна уу.",
              },
            })}
          />
        </FormWrapper>

        {/* phone */}
        <FormWrapper label={labels.phone} errorMsg={errors?.phone}>
          <input
            className={errors?.phone ? inputErrorStyle : inputStyle}
            name="phone"
            defaultValue={0}
            placeholder={"XXXX-XXXX"}
            {...register("phone", {
              required: { value: true, message: "Заавал бөглөнө үү" },
              validate: {
                number: (v) => v > 0 || "Заавал тоо оруулна уу.",
                length: (v) =>
                  v.toString().length === 8 || "Зөв дугаар оруулна уу.",
              },
            })}
          />
        </FormWrapper>

        {/* Organization name */}
        <FormWrapper label={labels.orgName} errorMsg={errors?.orgName}>
          <input
            className={errors?.orgName ? inputErrorStyle : inputStyle}
            name="orgName"
            placeholder={labels.orgName}
            {...register("orgName", {
              required: { value: true, message: "Заавал бөглөнө үү" },
            })}
          />
        </FormWrapper>

        {/* Operation */}
        <FormWrapper label={labels.operationDir} errorMsg={errors?.operation}>
          <input
            className={errors?.operation ? inputErrorStyle : inputStyle}
            name="operation"
            placeholder={labels.operationDir}
            {...register("operation", {
              required: { value: true, message: "Заавал бөглөнө үү" },
            })}
          />
        </FormWrapper>
      </div>
      <SubmitButton
        btnName={btnName}
        isDisabled={submitBtnDisabled}
        loadingSend={loadingSend}
      />
    </form>
  );
};

export default ShortForm;
