import React, { useState } from "react";

import emailjs from "@emailjs/browser";
import { toast } from "react-toastify";
import { useForm } from "react-hook-form";

import {
  FormWrapper,
  inputErrorStyle,
  inputStyle,
  SubmitButton,
} from "@/components/quotation/shared";

const LongForm = ({ btnName, labels }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm();

  const [loadingSend, setLoadingSend] = React.useState(false);
  const [submitBtnDisabled, setSubmitBtnDisabled] = React.useState(false);

  const onSubmit = async (data) => {
    const templateParams = data;
    setLoadingSend(true);
    setSubmitBtnDisabled(true);
    await emailjs
      .send(
        "service_y8mqtl3",
        "template_2krfpg2",
        templateParams,
        "gSuD5_a7MZhrv4alE"
      )
      .then(
        (response) => {
          reset();
          setLoadingSend(false);
          if (response?.status === 200) {
            toast.success("Таны и-мэйл амжилттай илгээгдлээ.", {
              position: "top-right",
              autoClose: 3000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
              theme: "dark",
            });
          }
          // console.log("SUCCESS!", response.status, response.text);
        },
        (err) => {
          setLoadingSend(false);
          // console.log("FAILED...", err);
          toast.error("Амжилтгүй", {
            position: "top-right",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "dark",
          });
        }
      );
    await setSubmitBtnDisabled(false);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="grid md:grid-cols-3 sm:grid-cols-2 md:gap-x-10 sm:gap-x-5 gap-y-3">
        {/* Firstname */}
        <FormWrapper
          label={labels.name} // translation comes here
          errorMsg={errors?.firstname}
        >
          <input
            className={errors?.firstname ? inputErrorStyle : inputStyle}
            name="firstname"
            placeholder={labels.name} // translation comes here
            {...register("firstname", {
              required: { value: true, message: "Заавал бөглөнө үү" },
            })}
          />
        </FormWrapper>

        {/* lastname */}
        <FormWrapper
          label={labels.lastname} // translation comes here
          errorMsg={errors?.lastname}
        >
          <input
            className={errors?.lastname ? inputErrorStyle : inputStyle}
            name={labels.lastname}
            placeholder={"lastname"} // translation comes here
            {...register("lastname", {
              required: { value: true, message: "Заавал бөглөнө үү" },
            })}
          />
        </FormWrapper>

        {/* mail */}
        <FormWrapper label={labels.mail} errorMsg={errors?.mail}>
          <input
            className={errors?.mail ? inputErrorStyle : inputStyle}
            name="mail"
            placeholder={labels.mail}
            {...register("mail", {
              required: { value: true, message: "Заавал бөглөнө үү" },
              pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                message: "Зөв и-мэйл оруулна уу.",
              },
            })}
          />
        </FormWrapper>

        {/* phone */}
        <FormWrapper label={labels.phone} errorMsg={errors?.phone}>
          <input
            className={errors?.phone ? inputErrorStyle : inputStyle}
            name="phone"
            defaultValue={0}
            placeholder={"XXXX-XXXX"}
            {...register("phone", {
              required: { value: true, message: "Заавал бөглөнө үү" },
              validate: {
                number: (v) => v > 0 || "Заавал тоо оруулна уу.",
                length: (v) =>
                  v.toString().length === 8 || "Зөв дугаар оруулна уу.",
              },
            })}
          />
        </FormWrapper>
        {/* Organization name */}
        <FormWrapper label={labels.orgName} errorMsg={errors?.orgName}>
          <input
            className={errors?.orgName ? inputErrorStyle : inputStyle}
            name="orgName"
            placeholder={labels.orgName}
            {...register("orgName", {
              required: { value: true, message: "Заавал бөглөнө үү" },
            })}
          />
        </FormWrapper>

        {/* Operation */}
        <FormWrapper label={labels.operationDir} errorMsg={errors?.operation}>
          <input
            className={errors?.operation ? inputErrorStyle : inputStyle}
            name="operation"
            placeholder={labels.operationDir}
            {...register("operation", {
              required: { value: true, message: "Заавал бөглөнө үү" },
            })}
          />
        </FormWrapper>
        {/* Эргэлтийн хөрөнгийн дүн /Total current assets*/}

        <FormWrapper
          req={false}
          label={labels.currentAsset}
          errorMsg={errors?.currentAsset}
        >
          <input
            className={errors?.currentAsset ? inputErrorStyle : inputStyle}
            name="currentAsset"
            placeholder={"0"}
            {...register("currentAsset")}
          />
        </FormWrapper>

        {/* Эргэлтийн бус хөрөнгийн дүн /Total non-current assets*/}
        <FormWrapper
          req={false}
          label={labels.nonCurrentAsset}
          errorMsg={errors?.nonCurrentAsset}
        >
          <input
            className={errors?.nonCurrentAsset ? inputErrorStyle : inputStyle}
            name="nonCurrentAsset"
            placeholder={"0"}
            {...register("nonCurrentAsset")}
          />
        </FormWrapper>

        {/* Богино хугацаат өр төлбөрийн дүн /Total short term payable */}
        <FormWrapper
          req={false}
          label={labels.shortTermPayable}
          errorMsg={errors?.shortTermPayable}
        >
          <input
            className={errors?.shortTermPayable ? inputErrorStyle : inputStyle}
            name="shortTermPayable"
            placeholder={"0"}
            {...register("shortTermPayable", {})}
          />
        </FormWrapper>

        {/* Урт хугацаат өр төлбөрийн дүн /Total long term payable */}
        <FormWrapper
          req={false}
          label={labels.longTermPayable}
          errorMsg={errors?.longTermPayable}
        >
          <input
            className={errors?.longTermPayable ? inputErrorStyle : inputStyle}
            name="longTermPayable"
            placeholder={"0"}
            {...register("longTermPayable", {})}
          />
        </FormWrapper>

        {/* Нийт эздийн өмчийн дүн /Total owners equity */}
        <FormWrapper
          req={false}
          label={labels.ownerEquity}
          errorMsg={errors?.ownerEquity}
        >
          <input
            className={errors?.ownerEquity ? inputErrorStyle : inputStyle}
            name="ownerEquity"
            placeholder={"0"}
            {...register("ownerEquity")}
          />
        </FormWrapper>

        {/* Борлуулалтын орлого /Revenue */}
        <FormWrapper
          label={labels.revenue}
          req={false}
          errorMsg={errors?.revenue}
        >
          <input
            className={errors?.revenue ? inputErrorStyle : inputStyle}
            name="revenue"
            placeholder={"0"}
            {...register("revenue", {})}
          />
        </FormWrapper>

        {/* Борлуулсан бүтээгдэхүүний өртөг /Cost of good sales */}
        <FormWrapper
          req={false}
          label={labels.costGoods}
          errorMsg={errors?.costGoods}
        >
          <input
            className={errors?.costGoods ? inputErrorStyle : inputStyle}
            name="costGoods"
            placeholder={"0"}
            {...register("costGoods", {})}
          />
        </FormWrapper>

        {/* Ерөнхий удирдлагын зардал /General and administration expense */}
        <FormWrapper
          req={false}
          label={labels.adminExpense}
          errorMsg={errors?.adminExpense}
        >
          <input
            className={errors?.adminExpense ? inputErrorStyle : inputStyle}
            name="adminExpense"
            placeholder={"0"}
            {...register("adminExpense", {})}
          />
        </FormWrapper>

        {/* Борлуулалт маркетингийн зардал /Sales and marketing expense */}
        <FormWrapper
          req={false}
          label={labels.salesExpense}
          errorMsg={errors?.salesExpense}
        >
          <input
            className={errors?.salesExpense ? inputErrorStyle : inputStyle}
            name="salesExpense"
            placeholder={"0"}
            {...register("salesExpense", {})}
          />
        </FormWrapper>

        {/* Бусад зардал /Other expense */}
        <FormWrapper
          req={false}
          label={labels.otherExpense}
          errorMsg={errors?.otherExpense}
        >
          <input
            className={errors?.otherExpense ? inputErrorStyle : inputStyle}
            name="otherExpense"
            placeholder={"0"}
            {...register("otherExpense", {})}
          />
        </FormWrapper>

        {/* Татварын зардал /tax expense */}
        <FormWrapper
          label={labels.taxExpense}
          req={false}
          errorMsg={errors?.taxExpense}
        >
          <input
            className={errors?.taxExpense ? inputErrorStyle : inputStyle}
            name="taxExpense"
            placeholder={"0"}
            {...register("taxExpense", {})}
          />
        </FormWrapper>
      </div>
      <SubmitButton
        btnName={btnName}
        isDisabled={submitBtnDisabled}
        loadingSend={loadingSend}
      />
    </form>
  );
};

export default LongForm;
