import Label from "@/lib/form/label";
import Spining from "@/lib/spining";

export function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export const FormWrapper = ({ label, children, req = true, errorMsg }) => {
  return (
    <div className="">
      <Label req={req} text={label} />
      {children}
      {errorMsg && errorMsg?.message ? (
        <p className="text-base text-red-400">{errorMsg?.message}</p>
      ) : (
        <p className="text-base text-transparent">hello</p>
      )}
    </div>
  );
};
export const SubmitButton = ({ isDisabled, loadingSend, btnName }) => {
  return (
    <div className="mt-5 flex justify-center items-center">
      <button
        type="submit"
        disabled={isDisabled}
        className="flex flex-row items-center px-10 py-3 text-base font-medium text-white bg-sky-600 transition ease-in-out shadow rounded-full hover:bg-cyan-500 hover:scale-105"
      >
        {loadingSend && <Spining />}
        {btnName}
      </button>
    </div>
  );
};

export const inputStyle =
  "text-base bg-slate-700 px-4 py-2 rounded w-full border-none outline-none text-slate-300 mb-1";

export const inputErrorStyle =
  "text-base bg-slate-700 px-4 py-2 rounded w-full text-slate-300 outline-red-400/50 outline outline-offset-2 border-none mb-1";

// Эргэлтийн хөрөнгийн дүн /Total current assets
// Эргэлтийн бус хөрөнгийн дүн /Total non-current assets
// Богино хугацаат өр төлбөрийн дүн /Total short term payable
// Урт хугацаат өр төлбөрийн дүн /Total long term payable
// Нийт эздийн өмчийн дүн /Total owners equity
// Борлуулалтын орлого /Revenue
// Борлуулсан бүтээгдэхүүний өртөг /Cost of good sales
// Ерөнхий удирдлагын зардал /General and administration expense
// Борлуулалт маркетингийн зардал /Sales and marketing expense
// Бусад зардал /Other expense
// Татварын зардал /tax expense

// shared: Short form
// Нэр: firstname,
// Овог: lastname,
// И-мэйл хаяг: mail,
// Утасны дугаар: phone,
// Байгууллагын нэр: orgName,
// Үйл ажиллагааны чиглэл: operation

// Long form
// Эргэлтийн хөрөнгийн дүн: currentAsset
// Эргэлтийн бус хөрөнгийн дүн: nonCurrentAsset
// Богино хугацаат өр төлбөрийн дүн: shortTermPayable
// Урт хугацаат өр төлбөрийн дүн: longTermPayable
// Нийт эздийн өмчийн дүн: ownerEquity
// Борлуулалтын орлого: revenue
// Борлуулсан бүтээгдэхүүний өртөг: costGoods
// Ерөнхий удирдлагын зардал: adminExpense
// Борлуулалт маркетингийн зардал: salesExpense
// Бусад зардал: otherExpense
// Татварын зардал: taxExpense
