import React from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import Seo from "@/lib/seo";
import Navigation from "@/lib/navigation/index";
import Footer from "@/lib/footer";
import Header from "@/lib/header";

import ScrollTopButton from "@/lib/scrollTopButton";
import { useTranslation } from "next-i18next";

const Certificate = () => {
  const { t: tMargins } = useTranslation("margins");

  return (
    <div>
      <Seo title="Гэрчилгээ" url="/cert" />
      <Navigation activeNav="/cert" />
      <div className="w-full min-h-screen text-gray-600 relative">
        <Header
          title={tMargins("cert")}
          img={true}
          imgs={{
            high: "/header/high/finance.jpeg",
            blur: "/header/low/finance.jpeg",
          }}
        />
        <div className="relative w-full min-h-[50vh] lg:px-40 px-10 py-16 bg-bgdark text-gray-500 text-base ">
          Гэрчилгээ
        </div>
      </div>
      <ScrollTopButton />
      <Footer />
    </div>
  );
};

export default Certificate;

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      locale,
      ...(await serverSideTranslations(locale, [
        "margins",
        "navigation",
        "qoutation",
      ])),
    },
  };
};
