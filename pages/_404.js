import CustomError from "@/lib/customError";
import React from "react";

const NotFound = () => {
  return <CustomError />;
};

export default NotFound;
