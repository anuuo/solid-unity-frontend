import React, { useState } from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import Seo from "@/lib/seo";
import Navigation from "@/lib/navigation/index";
import Footer from "@/lib/footer";
import Header from "@/lib/header";

import { SubTitleWhite } from "@/lib/title";
import { useTranslation } from "next-i18next";
import QuotationBanner from "@/components/quotation_banner";
import BarChart from "@/components/barChart";
import ScrollTopButton from "@/lib/scrollTopButton";

const Subtitle = (props) => {
  return (
    <p className="sm:text-lg text-base font-extralight tracking-wide  text-right py-1 ">
      {props.children}
    </p>
  );
};
const Title = (props) => (
  <p className="md:text-2xl text-md  font-extralight text-sky-500">
    {props.children}
  </p>
);

const Text = (props) => <p className="py-4 text-lg text-justify">{props.children}</p>;

const Section = ({ id = "01", title = "Title", data = [] }) => {
  const [key, setKey] = useState(0);

  // React.useEffect(() => {
  //   setvalue(intialData);
  //   console.log("changing...", intialData?.title, data[0]?.title);
  //   return () => {};
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [locale]);

  // console.log("intialData", data[0], intialData);
  return (
    <div className="flex sm:flex-row flex-col sm:gap-10 lg:gap-20 w-full ">
      <div className="sm:w-1/3 w-full flex flex-col justify-start">
        <p className="lg:text-[15em] lg:leading-[270px] md:text-[10em] text-[150px] leading-[150px] font-bold p-0 m-0 text-right ">
          {id}
        </p>
        <div className="pb-2 sm:hidden block">
          <p className="text-right text-3xl font-extralight text-sky-500">
            {title}
          </p>
        </div>
        {data?.map((item, id) => {
          const isActive = item?.title === data[key]?.title;
          return (
            <button
              key={id}
              onClick={() => setKey(id)}
              className={
                isActive
                  ? "sm:hover:text-sky-400 transition-all ease-in-out text-white sm:text-sky-500"
                  : "sm:hover:text-sky-500 transition-all ease-in-out text-slate-400 "
              }
            >
              <Subtitle>{item?.title}</Subtitle>
            </button>
          );
        })}
      </div>
      <div className="sm:w-2/3 w-full flex flex-col py-8">
        <div className="sm:block hidden">
          <Title>{title}</Title>
        </div>
        <div>
          <p className="lg:text-5xl sm:text-4xl text-3xl py-4 font-extralight text-white">
            {data[key]?.title}
          </p>
          <div className="h-1 w-1/5 bg-sky-500 rounded-lg mb-4" />
        </div>
        <div>
          {typeof data[key]?.para === "object" &&
            data[key]?.para?.map((item, key) => {
              return <Text key={key}>{item}</Text>;
            })}
        </div>
      </div>
    </div>
  );
};

const Service = (props) => {
  const { t } = useTranslation("service");

  return (
    <div>
      <Seo title="Үйлчилгээ" url="/service" />
      <Navigation activeNav="/service" locale={props?.locale} />

      <div className="w-full min-h-screen text-gray-600 relative">
        <Header path="/service" />

        {/* <div className="pb-10">
            <SubTitleLg color="gray-800">{t("title")}</SubTitleLg>
            <p>{t("desc")}</p>
          </div> */}
        <div className="w-full lg:px-40 md:px-30 px-10 py-16 bg-bgdark text-gray-300 relative">
          <Section
            title={t("audit", { returnObjects: true })?.title ?? "Title"}
            data={t("audit", { returnObjects: true })?.value ?? []}
          />
        </div>
        <div className="w-full lg:px-40 md:px-30 px-10 py-16 bg-bglight text-gray-300 relative">
          <Section
            id="02"
            title={t("consult", { returnObjects: true })?.title ?? "Title"}
            data={t("consult", { returnObjects: true })?.value ?? []}
          />
        </div>
        {/* <div className="w-full lg:px-40 md:px-30 px-10 py-16 bg-bgdark text-gray-300 relative">
          <Section
            id="03"
            title={t("valuation", { returnObjects: true })?.title ?? "Title"}
            data={t("valuation", { returnObjects: true })?.value ?? []}
          />
        </div> */}
        {/* <div className="w-full lg:px-40 md:px-30 px-10 py-16 bg-bglight text-gray-300 relative">
          <div className="text-left flex sm:flex-row flex-col justify-between items-center gap-10">
            <p className="text-slate-50 font-extralight sm:text-base text-base">
              Бид өнгөрсөн хугацаанд эрчим хүч, худалдаа, үйлдвэрлэл үйлчилгээ,
              уул уурхай, харилцаа холбоо мэдээлэл, санхүү, даатгал, авто зам,
              барилгын салбарын Төрийн өмчийн, хувийн хэвшлийн, олон улсын төсөл
              хөтөлбөр зэрэг аж ахуйн нэгж, байгууллага, төслийн нэгжид
              үйлчилгээ үзүүлсэн бөгөөд эдгээр салбар бүрийн төлөөллүүдтэй
              амжилттай хамтарч ажилласнаар бидний мэдлэг туршлага мөн
              нэмэгдсээр ирсэн билээ.
            </p>
            <p className="text-slate-50 font-extralight sm:text-base text-base">
              Хэдийгээр манай үйлчлүүлэгч бүр өөрийн гэсэн онцлогтой, хоорондоо
              ялгаатай боловч тэднийг нэгтгэж байдаг нэг зүйл нь бид бүгдээрээ
              нягт хамтын ажиллагааг дэмжин ажилладагт оршдог юм. Бид өөрсдийн
              мэргэжлийн мэдлэг, ур чадвараас гадна үйлчлүүлэгчидтэйгээ хамт
              байж асуудлыг хамтдаа шийдвэрлэж чадна гэдэгтээ итгэлтэй байдаг.
              Бид өнөөдрийн байдлаар нийт 98 харилцагчид үйлчилгээ үзүүлсэн
              байна.
            </p>
          </div>
        </div> */}
        <div className="w-full lg:px-40 md:px-30 px-10 py-20 bg-bgdark text-gray-300 relative text-center">
          <div className="py-6">
            <SubTitleWhite>
              {t("customerNumber", { returnObjects: true })?.title}
            </SubTitleWhite>
          </div>
          <BarChart locale={props?.locale} />
        </div>

        <div className="relative bg-bglight">
          <QuotationBanner locale={props.locale} />
        </div>
        {/* </div> */}
      </div>
      <ScrollTopButton />
      <Footer />
    </div>
  );
};

export default Service;

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      locale,
      ...(await serverSideTranslations(locale, [
        "margins",
        "navigation",
        "service",
        "quotation",
      ])),
    },
  };
};
