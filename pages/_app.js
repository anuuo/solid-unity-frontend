import "../styles/globals.css";
import ErrorBoundary, { ErrorFallback } from "@/lib/errrorboundary";
import { appWithTranslation } from "next-i18next";
import { Analytics } from "@vercel/analytics/react";
import Maintenance from "@/lib/maintenance";

function MyApp({ Component, pageProps }) {
  return (
    <ErrorBoundary FallbackComponent={ErrorFallback}>
      <Component {...pageProps} />
      {/* <Maintenance /> */}
      <Analytics />
    </ErrorBoundary>
  );
}

export default appWithTranslation(MyApp);
