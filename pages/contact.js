import React from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import Seo from "@/lib/seo";
import Navigation from "@/lib/navigation/index";
import Footer from "@/lib/footer";
import Header from "@/lib/header";

import ContactComp from "@/components/contact";
import ScrollTopButton from "@/lib/scrollTopButton";

const Contact = (props) => {
  return (
    <div>
      <Seo title="Холбоо барих" url="/contact" />
      <Navigation activeNav="/contact" locale={props.locale} />
      <div className="w-full min-h-screen text-gray-600 relative">
        <Header path="/contact" />
        <div className="relative w-full lg:px-40 px-10 py-16 bg-bglight text-gray-500 text-base">
          <ContactComp />
        </div>
      </div>
      <ScrollTopButton />
      <Footer />
    </div>
  );
};

export default Contact;
export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      locale,
      ...(await serverSideTranslations(locale, [
        "margins",
        "navigation",
        "qoutation",
        "contact",
        "mail",
        // "person4",
      ])),
    },
  };
};
