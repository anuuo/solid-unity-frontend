import React from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import Seo from "@/lib/seo";
import Navigation from "@/lib/navigation/index";
import Footer from "@/lib/footer";
import Header from "@/lib/header";
import { useTranslation } from "next-i18next";
import { Transition } from "@headlessui/react";
import ScrollTopButton from "@/lib/scrollTopButton";

import { Title, ListView, DownloadButton } from "@/components/hr/micro";
import JobIntroduction from "@/components/hr/jobIntroduction";
import { useRouter } from "next/router";
// import downloadFile from "./api/downloadFile";

const Card = ({
  locale = "mn",
  title = "",
  type = "",
  hour = "",
  mission = "",
  requirement = [],
  formData = {},
  moreBtn = "",
  shortBtn = "",
  handleDownload = () => {},
  // isVisible = false,
}) => {
  const [isVisible, setIsVisible] = React.useState(false);
  return (
    <div className="text-slate-300 bg-slate-800 rounded-2xl md:p-10 px-5 py-8 sm:text-base text-base">
      <JobIntroduction {...{ title, hour, type, mission }} />

      {/* Desktop VIEW */}
      <div className="sm:block hidden">
        <div className="flex flex-col gap-4">
          {requirement?.map((item, key) => {
            return (
              <div key={key}>
                <Title>{item?.title}:</Title>
                <div className="sm:pl-8 sm:py-0 py-1">
                  {item?.value?.map((item, key) => {
                    return <ListView {...{ item }} key={key} />;
                  })}
                </div>
              </div>
            );
          })}
        </div>
        <div className="pt-4">
          <Title>{formData?.title}</Title>
          <div className="sm:pl-8 sm:py-0 py-1">
            {formData?.value?.map((item, key) => {
              return <ListView {...{ item }} key={key} />;
            })}
          </div>
        </div>
        <DownloadButton locale={locale} handleDownload={handleDownload} />
      </div>

      <Transition
        appear={isVisible}
        show={isVisible}
        enter="transition-opacity duration-75"
        enterFrom="opacity-0"
        enterTo="opacity-100"
        leave="transition-opacity duration-150"
        leaveFrom="opacity-100"
        leaveTo="opacity-0"
      >
        {/* {isVisible && ( */}
        <div className="block py-6">
          <div className="flex-col gap-4">
            {requirement?.map((item, key) => {
              return (
                <div key={key} className="pb-6">
                  <Title>{item?.title}:</Title>
                  <div className="pl-4">
                    {item?.value?.map((item, key) => {
                      return <ListView {...{ item }} key={key} />;
                    })}
                  </div>
                </div>
              );
            })}
          </div>
          <div className="pt-4">
            <Title>{formData?.title}</Title>
            <div className="pl-4">
              {formData?.value?.map((item, key) => {
                return <ListView {...{ item }} key={key} />;
              })}
            </div>
          </div>
          <div className="pt-2">
            <DownloadButton locale={locale} handleDownload={handleDownload} />
          </div>
        </div>

        {/* )} */}
      </Transition>

      <div className="flex justify-start sm:hidden">
        <button
          onClick={() => setIsVisible((prev) => !prev)}
          className={
            isVisible
              ? "text-teal-500 border-2 border-teal-500 rounded-lg px-4 py-1"
              : "text-sky-500 border-2 border-sky-500 rounded-lg px-4 py-1"
          }
        >
          {isVisible ? shortBtn : moreBtn}
        </button>
      </div>
    </div>
  );
};

const Humanresource = (props) => {
  const router = useRouter;
  const { t } = useTranslation("hr");
  const auditorData = t("auditor", { returnObjects: true });
  const assistantData = t("assistant", { returnObjects: true });
  const formData = t("form", { returnObjects: true });

  const moreBtn = props?.locale === "mn" ? "Дэлгэрэнгүй" : "More";
  const shortBtn = props?.locale === "mn" ? "Багасгах" : "Minimize";

  const handleDownload = () => {};
  return (
    <div>
      <Seo title="Хүний Нөөц" url="/humanresource" />
      <Navigation activeNav="/humanresource" locale={props?.locale ?? "mn"} />
      <div className="w-full min-h-screen text-gray-600 relative bg-bgdark">
        <Header path="/humanresource" />
        <div className="w-full h-full lg:px-40 md:px-30 px-8 py-16 bg-bgdark text-gray-300 relative">
          <div className="flex flex-col gap-10 ">
            <Card
              {...{
                locale: props?.locale,
                title: auditorData?.title,
                type: t("type"),
                hour: t("hour"),
                mission: auditorData?.mission,
                requirement: auditorData?.requirement,
                formData,
                moreBtn,
                shortBtn,
                handleDownload,
              }}
            />
            <Card
              {...{
                handleDownload,
                locale: props?.locale,
                title: assistantData?.title,
                type: t("type"),
                hour: t("hour"),
                mission: assistantData?.mission,
                requirement: assistantData?.requirement,
                formData,
                moreBtn,
                shortBtn,
              }}
            />
          </div>
        </div>
      </div>
      <ScrollTopButton />
      <Footer />
    </div>
  );
};

export default Humanresource;

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      locale,
      ...(await serverSideTranslations(locale, [
        "margins",
        "navigation",
        "qoutation",
        "hr",
        // "person4",
      ])),
    },
  };
};
