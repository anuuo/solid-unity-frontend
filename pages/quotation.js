import React from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import Seo from "@/lib/seo";
import Navigation from "@/lib/navigation/index";
import Footer from "@/lib/footer";
import Header from "@/lib/header";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import { Tab } from "@headlessui/react";
import { SubTitleLg } from "@/lib/title";
import ShortForm from "@/components/quotation/shortForm";
import LongForm from "@/components/quotation/longForm";
import { classNames } from "@/components/quotation/shared";
import ScrollTopButton from "@/lib/scrollTopButton";
import { useTranslation } from "next-i18next";

const Quotation = ({ locale }) => {
  const { t } = useTranslation("quotation");
  return (
    <div>
      <Seo title="Үнийн санал" url="/quotation" />
      <Navigation activeNav="/quotation" locale={locale} />
      <ToastContainer />
      <div className="w-full min-h-screen text-gray-600 relative">
        <Header path="/quotation" />
        <div className="w-full lg:px-40 md:px-12 px-10 py-16 bg-bgdark text-gray-300 relative min-h-screen">
          <div className="pb-10">
            <div className="text-center">
              <SubTitleLg>{t("title")}</SubTitleLg>
            </div>
            <p className="text-center">{t("text")}</p>
          </div>
          <Tab.Group>
            <Tab.List className="flex space-x-1 rounded-xl bg-slate-600/20 p-1">
              <Tab
                className={({ selected }) =>
                  classNames(
                    "w-full rounded-lg py-2.5 text-base font-medium leading-5 text-blue-300",
                    selected
                      ? "bg-slate-600/50 shadow"
                      : "text-blue-100 hover:bg-slate-600/10 hover:text-white"
                  )
                }
              >
                {t("form.titles.brief")}
              </Tab>
              <Tab
                className={({ selected }) =>
                  classNames(
                    "w-full rounded-lg py-2.5 text-base font-medium leading-5 text-blue-300",
                    selected
                      ? "bg-slate-600/50 shadow"
                      : "text-blue-100 hover:bg-white/[0.12] hover:text-white"
                  )
                }
              >
                {t("form.titles.detail")}
              </Tab>
            </Tab.List>
            <Tab.Panels>
              <Tab.Panel
                className={classNames(
                  "rounded-xl bg-slate-600/20 md:p-10 sm:p-5 p-5 mt-4 "
                )}
              >
                <ShortForm
                  btnName={locale === "mn" ? "Илгээх" : "Send"}
                  labels={t("form", { returnObjects: true })}
                />
              </Tab.Panel>
              <Tab.Panel
                className={classNames(
                  "rounded-xl bg-slate-600/20 md:p-10 sm:p-5 p-5 mt-4 "
                )}
              >
                <LongForm
                  btnName={locale === "mn" ? "Илгээх" : "Send"}
                  labels={t("form", { returnObjects: true })}
                />
              </Tab.Panel>
            </Tab.Panels>
          </Tab.Group>
        </div>
      </div>
      <ScrollTopButton />
      <Footer />
    </div>
  );
};

export default Quotation;
export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      locale,
      ...(await serverSideTranslations(locale, [
        "margins",
        "navigation",
        "service",
        "quotation",
      ])),
    },
  };
};
