import CustomError from "@/lib/customError";
import Link from "next/link";
import React from "react";

const Error = () => {
  return <CustomError />;
};

export default Error;
