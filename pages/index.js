import Seo from "@/lib/seo";
import Navigation from "@/lib/navigation";

import Service from "@/components/service";
import Landingpage from "@/components/landingpage";
import Etiquette from "@/components/etiquette";
import Partners from "@/components/partners";
import Footer from "@/lib/footer";
import Contact from "@/components/contact";
import Cusfeedback from "@/components/cusfeedback";

import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import QuotationBanner from "@/components/quotation_banner";
import ScrollTopButton from "@/lib/scrollTopButton";
import { ContainerX } from "@/lib/layout";

function Home(props) {
  return (
    <div>
      <Seo title="Нүүр" />
      <Navigation activeNav="/" locale={props?.locale ?? "mn"} />
      <Landingpage locale={props.locale} />
      <Etiquette />
      <Service locale={props.locale} />
      {/* <Cusfeedback /> */}
      <div className="relative bg-bgdark">
        <QuotationBanner locale={props.locale} />
      </div>

      <div className="relative w-full py-24 bg-bglight text-gray-500 text-base text-center">
        <ContainerX>
          <Contact />
        </ContainerX>
      </div>
      <ScrollTopButton />
      {/* <Partners /> */}
      <Footer />
    </div>
  );
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      locale,
      ...(await serverSideTranslations(locale, [
        "margins",
        "home",
        "navigation",
        "ethic",
        "service",
        "feedback",
        "partner",
        "contact",
        "mail",
        "quotation",
      ])),
    },
  };
};

export default Home;
