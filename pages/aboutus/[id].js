import React from "react";
import Seo from "@/lib/seo";
import Navigation from "@/lib/navigation/index";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import Footer from "@/lib/footer";

import Image from "next/image";
import { useTranslation } from "next-i18next";

import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import { Navigation as SwiperNav, A11y } from "swiper";

import { useBreakpoints } from "@/hooks/index";
import { useRouter } from "next/router";
import { IdSliderCard } from "@/components/aboutusCard";
import ScrollTopButton from "@/lib/scrollTopButton";
import PersonSkill from "@/components/aboutus/personSkill";
import PersonEdu from "@/components/aboutus/personEdu";
import PersonBreadCrumb from "@/components/aboutus/personBreadCrumb";
import PersonJobExp from "@/components/aboutus/personJobExp";
import { PersonText } from "@/components/aboutus/personMicro";
import PersonMember from "@/components/aboutus/personMember";

const resolvePerview = (breakpoint) => {
  switch (breakpoint) {
    case "default":
      return 2;
    case "xs":
      return 3;
    case "sm":
      return 4;
    case "md":
      return 4;
    case "lg":
      return 5;
    case "xl":
      return 5;
    case "xxl":
      return 6;
    case "3xl":
      return 7;
    default:
      return 2;
  }
};

const Employee = ({ locale, personNum, params }) => {
  const router = useRouter();
  const { t } = useTranslation(`person${personNum}`);
  const { t: tDef } = useTranslation();

  const breakpoint = useBreakpoints();

  const handleClick = (id) => {
    // console.log("CLICKED HERE");
    router.push(`/aboutus/${id}`);
  };

  // console.log("person", t("experience", { returnObjects: true }));

  const education = t("education", { returnObjects: true });
  const experience = t("experience", { returnObjects: true });
  const para = t("para", { returnObjects: true });
  const skill = t("skill", { returnObjects: true });
  const membership = t("membership", { returnObjects: true });

  return (
    <div>
      <Seo title="Бидний тухай" url="/aboutus" />
      <Navigation activeNav="/aboutus" locale={locale} />
      <div className="w-full min-h-screen text-gray-300 relative bg-bgdark  flex flex-col items-center md:pt-32 pt-16 sm:pt-28">
        <div className="md:w-5/6 w-full text-left md:pb-4 pb-4 px-10 md:px-0">
          <PersonBreadCrumb
            locale={locale}
            name={t("name")}
            id={params?.id ?? 0}
          />
        </div>
        <div className="md:w-5/6 flex sm:flex-row flex-col justify-around md:gap-12 md:px-0 gap-10 px-10 pb-8">
          <div className="md:w-1/3 w-full md:h-full overflow-hidden">
            <Image
              width={861}
              height={1226}
              placeholder="blur"
              layout="responsive"
              alt={t("name")}
              src={`/team${t("img.high")}`}
              blurDataURL={`/team${t("img.low")}`}
              className="rounded-full sm:aspect-[5/6] aspect-[6/7] object-cover object-top"
            />
            {/* SKILL */}
            <div className="pt-8 md:block hidden">
              <PersonSkill skill={skill} />
            </div>
          </div>
          <div className="text-gray-300 md:w-2/3 w-full">
            {/* INTRODUCTION */}
            <div className="">
              <p className="lg:text-4xl font-semibold text-2xl pb-4 text-white capitalize">
                {locale === "mn" ? t("lastname") : t("name")}{" "}
                {locale === "mn" ? t("name") : t("lastname")}
              </p>
              <p className="sm:text-2xl text-sky-500 text-xl sm:pb-1 pb-1">
                {t("job")}
              </p>
              <p className="sm:text-xl text-lg text-gray-300/90 pb-4">
                {t("rank")}
              </p>
            </div>

            {/* skill */}
            <div className="py-4 md:hidden block">
              <PersonSkill skill={skill} />
            </div>

            {/* EDUCATION */}
            <PersonEdu education={education} />

            {/* JOB EXPERIENCE */}
            <PersonJobExp experience={experience} />

            <div className="py-4 flex flex-col gap-2 md:pl-8 pl-0">
              {para &&
                para?.map((item, key) => {
                  return (
                    <div
                      key={key}
                      className=" indent-5 text-base py-[3px] flex flex-row"
                    >
                      <PersonText>{item}</PersonText>
                    </div>
                  );
                })}
            </div>
            {/* MEMBERSHIP */}
            <PersonMember membership={membership} />
          </div>
        </div>
        <div className="md:w-5/6 w-full text-left py-8 px-10 md:px-0">
          <Swiper
            navigation
            spaceBetween={10}
            slidesPerView={resolvePerview(breakpoint)}
            // scrollbar={{ draggable: true }}
            modules={[SwiperNav, A11y]}
            // grabCursor={true}
            autoplay={false}
            // showsButtons={false}
            className="overflow-hidden"
          >
            {Array(15)
              .fill()
              .map((item, id) => {
                return (
                  id !== 2 && (
                    <SwiperSlide
                      key={id}
                      className="flex justify-center items-center"
                      onClick={() => {
                        handleClick(id);
                      }}
                    >
                      <div className="xxl:h-[400px] xxl:w-[260px] xl:h-[350px] xl:w-[250px] md:h-[250px] md:w-[20vw] sm:h-[200px] sm:w-[20vw] xs:h-[300px] xs:w-[50vw] h-[200px] w-[150px]">
                        <IdSliderCard
                          {...{
                            id,
                            locale: locale ?? "mn",
                            name: tDef(`person${id + 1}:name`),
                            lastname: tDef(`person${id + 1}:lastname`),
                            job: tDef(`person${id + 1}:job`),
                            age: tDef(`person${id + 1}:age`),
                            btn: tDef(`person${id + 1}:btn`),
                            img: tDef(`person${id + 1}:img`, {
                              returnObjects: true,
                            }),
                            // handleCard,
                            slug: tDef(`person${id + 1}:name`),
                          }}
                        />
                      </div>
                    </SwiperSlide>
                  )
                );
              })}
          </Swiper>
        </div>
      </div>
      <ScrollTopButton />
      <Footer />
    </div>
  );
};

export async function getServerSideProps({ locale, params }) {
  const personNum = parseInt(params?.id) + 1;

  return {
    props: {
      params,
      locale,
      personNum,
      ...(await serverSideTranslations(locale, [
        "margins",
        "navigation",
        // `person${personNum}`,
        "person1",
        "person2",
        "person3",
        "person4",
        "person5",
        "person6",
        "person7",
        "person8",
        "person9",
        "person10",
        "person11",
        "person12",
        "person13",
        "person14",
        "person15",
      ])),
    }, // will be passed to the page component as props
  };
}
export default Employee;
