/* eslint-disable @next/next/no-img-element */
import React from "react";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import Image from "next/image";
import dynamic from "next/dynamic";

import Seo from "@/lib/seo";
import Header from "@/lib/header";
import Footer from "@/lib/footer";
import Navigation from "@/lib/navigation/index";
import { SubTitleLg, SubTitleWhite } from "@/lib/title";

import AboutusCard from "@/components/aboutusCard";
import { EtiquettePlain } from "@/components/etiquette";
import Timeline from "@/components/aboutus/timeline";

import { ParaText } from "@/lib/text";
import ScrollTopButton from "@/lib/scrollTopButton";
import NumberCustomerChart from "@/components/numberCustomerChart";
import QuotationBanner from "@/components/quotation_banner";
import { ContainerX } from "@/lib/layout";
import NewAboutusCard from "@/components/newAboutusCard";

const PieChartWithoutSSR = dynamic(import("@/components/aboutus/pieChart"), {
  ssr: false,
});
// import PieRechartComponent from "@/components/aboutus/pieChart";
// import BarChart from "@/components/aboutus/barChart";

const Aboutus = (props) => {
  const { t: tAboutus } = useTranslation("aboutus");
  const { t: tTeam } = useTranslation("team");
  const { t: tDef } = useTranslation();

  return (
    <div>
      <Seo title="Бидний тухай" url="/aboutus" />
      <Navigation activeNav="/aboutus" locale={props?.locale ?? "mn"} />
      <div className="w-full min-h-screen text-gray-600 relative">
        <Header path="/aboutus" />
        <div className="relative w-full pt-20 text-center bg-bgdark text-gray-300">
          <ContainerX>
            <div className="pb-40 text-left lg:min-h-[50vh] bg-bgdark">
              <img
                alt="team image"
                src="/team/high/group_down.jpg"
                className="hidden sm:block float-right rounded-md object-contain overflow-hidden sm:w-1/2 2xl:w-1/3 ml-10 mb-4 bg-slate-600"
              />
              <div className="sm:text-left sm:pl-4 text-center">
                <SubTitleWhite>{tAboutus("subTitle1")}</SubTitleWhite>
              </div>
              <div className="block sm:hidden pb-4">
                <Image
                  layout="responsive"
                  width={700}
                  height={430}
                  src="/team/high/group_down.jpg"
                  alt="team image"
                  placeholder="blur"
                  blurDataURL="/team/low/group_down.jpeg"
                  className="rounded-md overflow-hidden object-cover"
                />
              </div>
              {typeof tAboutus("introduction", { returnObjects: true }) ===
              "string" ? (
                <ParaText>{tAboutus("introduction")}</ParaText>
              ) : (
                tAboutus("introduction", { returnObjects: true })?.map(
                  (item, key) => {
                    return <ParaText key={key}>{item}</ParaText>;
                  }
                )
              )}
            </div>
          </ContainerX>

          <div className="py-24 bg-bglight">
            <ContainerX>
              <EtiquettePlain />
            </ContainerX>
          </div>
          {/* <div className="lg:px-40 md:px-30 px-4 py-20">
            <Timeline />
          </div> */}
          <div className="bg-bgdark sm:py-32 py-20">
            <ContainerX>
              <PieChartWithoutSSR />
            </ContainerX>
          </div>
          <div className="bg-bglight pt-20 pb-16">
            <ContainerX>
              <NumberCustomerChart locale={props?.locale} />
            </ContainerX>
          </div>
          <div className="bg-bgdark flex flex-col py-16">
            <div className="text-center pb-10">
              <SubTitleLg>{tTeam("title")}</SubTitleLg>
            </div>
            <div className="lg:px-28 sm:px-10 px-4">
              <div className="justify-between items-stretch sm:gap-10 gap-4 md:gap-4 grid md:grid-cols-4 sm:grid-cols-3 grid-cols-2">
                {/* <div className="justify-between items-stretch sm:gap-10 gap-4 md:gap-4 grid md:grid-cols-4 sm:grid-cols-3 grid-cols-2"> */}
                {/* <div className="justify-between items-stretch sm:gap-10 gap-4 md:gap-4 grid xl:grid-cols-5 md:grid-cols-4 sm:grid-cols-3 grid-cols-2"> */}
                {Array(15)
                  .fill()
                  .map((item, id) => {
                    return (
                      id !== 2 && (
                        <NewAboutusCard
                          // <AboutusCard
                          key={id}
                          {...{
                            id,
                            locale: props?.locale ?? "mn",
                            name: tDef(`person${id + 1}:name`),
                            lastname: tDef(`person${id + 1}:lastname`),
                            job: tDef(`person${id + 1}:job`),
                            age: tDef(`person${id + 1}:age`),
                            btn: tDef(`person${id + 1}:btn`),
                            img: tDef(`person${id + 1}:img`, {
                              returnObjects: true,
                            }),
                            // handleCard,
                            slug: tDef(`person${id + 1}:name`),
                          }}
                        />
                      )
                    );
                  })}
              </div>
            </div>
          </div>
          <div className="bg-bglight">
            <QuotationBanner locale={props?.locale} />
          </div>
        </div>
      </div>
      <ScrollTopButton />
      <Footer />
    </div>
  );
};

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      locale,
      ...(await serverSideTranslations(locale, [
        "margins",
        "navigation",
        "ethic",
        "aboutus",
        "team",
        "person1",
        "person2",
        "person3",
        "person4",
        "person5",
        "person6",
        "person7",
        "person8",
        "person9",
        "person10",
        "person11",
        "person12",
        "person13",
        "person14",
        "person15",
        "service",
        "quotation",
      ])),
    },
  };
};

export default Aboutus;
