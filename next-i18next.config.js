const path = require("path");

module.exports = {
  i18n: {
    defaultLocale: "mn",
    locales: ["mn", "en"],
    localeDetection: false,
    reloadOnPrerender: process.env.NODE_ENV === "development",
  },
  localePath: path.resolve("./public/locales"),

  // fallbackLng: {
  //   default: ["mn"],
  // },
};
