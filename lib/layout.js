export const ContainerX = (props) => {
  return <div className="lg:px-32 md:px-20 px-10">{props.children}</div>;
};
