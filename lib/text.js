export const ParaText = ({ indent = true, children }) => {
  //   console.log("indent", indent);
  //   if (indent === true) {
  //     <p className="pb-4 sm:text-base text-base text-slate-300/80 indent-6">
  //       {children}
  //     </p>;
  //   }
  return (
    <p className="text-lg text-slate-300 indent-4 text-justify">{children}</p>
  );
};
