import React from "react";
import Image from "next/image";

import { ScreenTitle, ScreenTitleLg } from "@/lib/title";
import styles from "@/styles/background.module.css";

// import aboutus from "@/public/aboutus_low.jpeg";
// import service from "@/public/service_low.jpeg";
// import humanresource from "@/public/humanresource_low.jpeg";
// import contact from "@/public/contact_low.jpeg";
import { useTranslation } from "next-i18next";

import building from "@/public/header/low/building.jpeg";
import building2 from "@/public/header/low/building2.jpeg";
import ocean from "@/public/header/low/ocean.jpeg";
import ocean1 from "@/public/header/low/ocean1.jpeg";
import ocean2 from "@/public/header/low/ocean2.jpeg";
import architecture from "@/public/header/low/architecture.jpeg";
import earth from "@/public/header/low/earth.jpeg";
import up from "@/public/header/low/up.jpeg";

const HeaderTitle = ({ path = "", title }) => {
  const { t } = useTranslation("navigation");

  if (title && title?.length > 0) {
    return <ScreenTitleLg>{title}</ScreenTitleLg>;
  }
  switch (path) {
    case "/aboutus":
      return (
        <div className="relative ">
          {/* <p className="md:text-6xl text-4xl text-gray-400 text-center font-medium tracking-wide capitalize">
            {t("aboutus")}
          </p> */}
          <ScreenTitleLg>{t("aboutus")}</ScreenTitleLg>
          {/* <div className="h-full absolute top-0 bottom-0 right-0 left-0 flex flex-row justify-center items-end ">
            <p className="text-slate-200 text-5xl tracking-wide font-thin">{t("aboutus")}</p>
          </div> */}
        </div>
      );
    case "/service":
      return <ScreenTitle>{t("service")}</ScreenTitle>;
    case "/humanresource":
      return <ScreenTitle>{t("humanresource")}</ScreenTitle>;
    case "/contact":
      return <ScreenTitleLg>{t("contact")}</ScreenTitleLg>;
    case "/quotation":
      return <ScreenTitleLg>{t("quotation")}</ScreenTitleLg>;

    default:
      return <ScreenTitle>Hello!</ScreenTitle>;
  }
};

const findImg = ({ path = "" }) => {
  switch (path) {
    case "/aboutus":
      return {
        low: building.src,
        high: "/header/high/building.jpeg",
        blur: "/header/low/building.jpeg",
      };
    // return { low: aboutus.src, high: "/aboutus.jpeg" };
    case "/service":
      return {
        low: ocean.src,
        high: "/header/high/ocean.jpeg",
        blur: "/header/low/ocean.jpeg",
      };
    // return { low: service.src, high: "/service.jpeg" };
    case "/humanresource":
      return {
        low: earth.src,
        high: "/header/high/earth.jpeg",
        blur: "/header/low/earth.jpeg",
      };
    // return { low: humanresource.src, high: "/humanresource.jpeg" };
    case "/contact":
      return {
        low: architecture.src,
        high: "/header/high/architecture.jpeg",
        blur: "/header/low/architecture.jpeg",
      };
    case "/quotation":
      return {
        low: building2.src,
        high: "/header/high/building2.jpeg",
        blur: "/header/low/building2.jpeg",
      };
    default:
      return {
        low: building.src,
        high: "/header/high/building.jpeg",
        blur: "/header/low/building.jpeg",
      };
  }
};

// const BackgroundImage = ({ path = "" }) => {
//   switch (path) {
//     case "/aboutus":
//       return <div className={styles.aboutusImg} />;
//     case "/service":
//       return <div className={styles.serviceImg} />;
//     case "/humanresource":
//       return <div className={styles.humanresourceImg} />;
//     case "/contact":
//       return <div className={styles.contactImg} />;
//     default:
//       return <div className={styles.defaultImg} />;
//   }
// };

const Header = ({ path = "", title = "", img = false, imgs = {} }) => {
  const images = !img ? findImg({ path }) : imgs;

  return (
    <div className="w-full md:h-[55vh] h-[35vh] overflow-hidden relative bg-bgdark">
      <div className="fixed top-0 bottom-0 overflow-scroll w-full md:h-[55vh] h-[35vh]">
        <img
          src={images.low}
          alt="low quality"
          objectposition="center"
          className="object-cover w-full md:h-[55vh] h-[35vh] fixed"
        />
        {/* <BackgroundImage path={path} /> */}

        <Image
          layout="fill"
          objectFit="cover"
          src={images.high}
          alt="about us image"
          objectPosition={"center"}
          placeholder="blur"
          blurDataURL={images.blur}
          className="absolute top-0 bottom-0 right-0 left-0 w-full h-full"
        />
      </div>
      <div className={styles.headerImgOverlay}>
        <HeaderTitle path={path} title={title} />
      </div>
    </div>
  );
};

export default Header;
