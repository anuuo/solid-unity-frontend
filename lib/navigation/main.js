import React from "react";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";

import logo from "@/assets/logo1.png";
import DesktopNav from "./desktopNav";
import MobileNav from "./mobileNav";

const topCSS = {
  top: 0,
  background: "#000e1c",
  transition: "0.3s ease-in-out",
};
const bottomCSS = {
  // top: 35,
  transition: "0.3s ease-in-out",
  // background: "#000e1c",
  background:
    "linear-gradient(to bottom, rgba(8, 14, 25, 0.2) 0%, rgba(8, 14, 25, 0) 100%)",
};

export default function MainNavigation({ activeNav, locale }) {
  const { t } = useTranslation("navigation");
  const router = useRouter();
  const [scrollY, setScrollY] = React.useState(0);

  React.useEffect(() => {
    function logit() {
      setScrollY(window.pageYOffset);
    }
    function watchScroll() {
      window.addEventListener("scroll", logit);
    }
    watchScroll();

    return () => {
      window.removeEventListener("scroll", logit);
    };
  }, []);

  const handleLanguage = (lan) => {
    router.push(
      {
        route: router.pathname,
        query: router.query,
      },
      router.asPath,
      { locale: lan }
    );
  };
  const handleHome = () => {
    router.push("/");
  };
  return (
    <nav
      style={scrollY > 50 ? topCSS : bottomCSS}
      className="top-0 sm:top-[44px] fixed z-50 w-full flex flex-row py-2 lg:px-28 px-4 bg-transparent text-base items-center text-gray-300 justify-between"
    >
      <button className="flex flex-row items-center" onClick={handleHome}>
        <img src={logo.src} alt="profile-img" className="object-fill h-10" />
        <p className="pl-2 font-medium text-white uppercase sm:text-base text-sm">
          {t("cpname")}
        </p>
      </button>

      <DesktopNav
        locale={locale}
        activeNav={activeNav}
        handleLanguage={handleLanguage}
      />

      <MobileNav
        locale={locale}
        activeNav={activeNav}
        handleLanguage={handleLanguage}
      />
    </nav>
  );
}
