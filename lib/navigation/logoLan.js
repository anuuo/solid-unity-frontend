const LogoLan = ({ lan }) => {
  return (
    <div className="flex flex-row items-center ">
      {/* <p className="pr-1 ">{lan === "mn" ? "MN" : "EN"}</p> */}
      {lan === "mn" ? (
        <img
          src="https://cdn.britannica.com/56/2756-004-54509464/Flag-Mongolia.jpg"
          alt="mn"
          className="w-10 h-4 object-contain"
        />
      ) : (
        <img
          alt="mn"
          src="https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/1200px-Flag_of_the_United_Kingdom.svg.png"
          className="w-10 h-4 object-contain"
        />
      )}
    </div>
  );
};
export default LogoLan;
