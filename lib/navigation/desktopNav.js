import React from "react";
import { useTranslation } from "next-i18next";

import { Menu } from "@headlessui/react";
import LogoLan from "./logoLan";
import Navigator from "./navigatorLink";
import { IoChevronDown } from "react-icons/io5";

const DesktopNav = ({ activeNav, locale, handleLanguage = () => {} }) => {
  const { t } = useTranslation("navigation");

  return (
    <div className="hidden md:flex flex-row items-center ">
      <Navigator link="/" text={t("home")} isActive={activeNav === "/"} />
      <Navigator
        text={t("aboutus")}
        link="/aboutus"
        isActive={activeNav === "/aboutus"}
      />
      <Navigator
        text={t("service")}
        link="/service"
        isActive={activeNav === "/service"}
      />
      <Navigator
        text={t("humanresource")}
        link="/humanresource"
        isActive={activeNav === "/humanresource"}
      />
      <Navigator
        text={t("contact")}
        link="/contact"
        isActive={activeNav === "/contact"}
      />
      <div className="pl-4">
        <Menu as="div" className="relative ">
          <div>
            <Menu.Button className="overflow-hidden flex flex-row items-center ">
              {locale === "mn" ? <LogoLan lan="mn" /> : <LogoLan lan="en" />}
              <IoChevronDown size={15} color="white" className="pt-0.5 pl-1" />
            </Menu.Button>
          </div>
          <Menu.Items className="absolute overflow-hidden top-10 right-0 origin-top-right dark:bg-gray-600 rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none flex flex-col w-24">
            <Menu.Item>
              {({ active }) => (
                <button
                  onClick={() => handleLanguage("en")}
                  className={`${
                    active && "bg-blue-500"
                  } px-4 flex flex-row items-center py-2`}
                >
                  <div className="flex flex-row items-center ">
                    <img
                      alt="en"
                      src="https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/1200px-Flag_of_the_United_Kingdom.svg.png"
                      className="w-10 h-4 object-contain"
                    />
                  </div>
                  <p href="/account-settings">EN</p>
                </button>
              )}
            </Menu.Item>
            <Menu.Item>
              {({ active }) => (
                <button
                  onClick={() => handleLanguage("mn")}
                  className={`${
                    active && "bg-blue-500"
                  } px-4 flex flex-row items-center justify-center py-2`}
                >
                  <div className="flex flex-row items-center">
                    <img
                      src="https://cdn.britannica.com/56/2756-004-54509464/Flag-Mongolia.jpg"
                      alt="mn"
                      className="w-10 h-4 object-contain"
                    />
                  </div>
                  <p>MN</p>
                </button>
              )}
            </Menu.Item>
          </Menu.Items>
        </Menu>
      </div>
    </div>
  );
};

export default DesktopNav;
