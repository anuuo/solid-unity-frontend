import React from "react";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";

import Link from "next/link";
import { HiMenuAlt3 } from "react-icons/hi";
import { Menu } from "@headlessui/react";

// eslint-disable-next-line react/display-name
const NavLink = React.forwardRef((props, ref) => {
  let { href, children, isActive, ...rest } = props;
  return (
    <Link href={href}>
      <p
        ref={ref}
        {...rest}
        className={`${isActive && "bg-slate-700"} py-2 text-center rounded px-8`}
        //  className={`px-4 py-2 text-center`}
      >
        {children}
      </p>
    </Link>
  );
});
const MobileNav = ({ activeNav, locale, handleLanguage = () => {} }) => {
  const { t } = useTranslation("navigation");

  return (
    <div className="flex md:hidden flex-row items-center">
      <Menu as="div" className="relative ">
        <Menu.Button className="overflow-hidden">
          <HiMenuAlt3 size={30} color="white" />
        </Menu.Button>
        <Menu.Items className="absolute overflow-hidden top-12 right-0 origin-top-right dark:bg-gray-600 rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none flex flex-col w-max ">
          <Menu.Item>
            <NavLink isActive={activeNav === "/"} href="/">
              {t("home")}
            </NavLink>
          </Menu.Item>
          <Menu.Item>
            <NavLink isActive={activeNav === "/aboutus"} href="/aboutus">
              {t("aboutus")}
            </NavLink>
          </Menu.Item>
          <Menu.Item>
            <NavLink isActive={activeNav === "/service"} href="/service">
              {t("service")}
            </NavLink>
          </Menu.Item>
          <Menu.Item>
            <NavLink
              isActive={activeNav === "/humanresource"}
              href="/humanresource"
            >
              {t("humanresource")}
            </NavLink>
          </Menu.Item>
          <Menu.Item>
            <NavLink isActive={activeNav === "/contact"} href="/contact">
              {t("contact")}
            </NavLink>
          </Menu.Item>

          <Menu.Item>
            {({ active }) =>
              locale === "en" ? (
                <button
                  onClick={() => handleLanguage("mn")}
                  className={`${active && "bg-blue-500"} px-4 py-2 text-center`}
                >
                  MN
                </button>
              ) : (
                <button
                  onClick={() => handleLanguage("en")}
                  className={`${active && "bg-blue-500"} px-4 py-2 text-center`}
                >
                  EN
                </button>
              )
            }
          </Menu.Item>
        </Menu.Items>
      </Menu>
    </div>
  );
};

export default MobileNav;
