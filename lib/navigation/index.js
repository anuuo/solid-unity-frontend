import React from "react";
import UpperNavigation from "@/lib/navigation/upper";
import MainNavigation from "@/lib/navigation/main";

export default function Navigation({ activeNav = "/", locale = "mn" }) {
  return (
    <React.Fragment>
      <UpperNavigation />
      <MainNavigation activeNav={activeNav} locale={locale} />
    </React.Fragment>
  );
}
