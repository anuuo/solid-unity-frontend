import React from "react";
import Link from "next/link";

const Navigator = ({ text, link, isActive = false }) => {
  return (
    <div>
      <p
        className={`px-4 hover:text-blue-300 text-gray-50 text-center text-base tracking-wide`}
      >
        <Link href={link}>{text}</Link>
      </p>
      <div
        className={`mt-1 h-[2px] rounded w-full ${
          isActive ? "bg-blue-400 " : "bg-transparent"
        }`}
      />
    </div>
  );
};

export default Navigator;
