import React from "react";
import { IoMdTime } from "react-icons/io";
import { BsFacebook, BsInstagram } from "react-icons/bs";
import { IoMailOpenOutline, IoCallOutline, IoLocation } from "react-icons/io5";
import { useTranslation } from "next-i18next";

export default function UpperNavigation() {
  const { t } = useTranslation("margins");
  return (
    <div className="fixed top-0 w-full hidden sm:flex py-3 lg:px-28 px-10 bg-[#000e1c] text-xs sm:text-sm flex-row items-center text-gray-300 justify-between z-[11]">
      <div className="flex flex-row items-center">
        <div className="hidden lg:flex flex-row items-center pr-4">
          <IoLocation size={20} color="gray" />
          <p className="pl-1 ">{t("upper1")}</p>
        </div>
        <div className="hidden md:flex flex-row items-center pr-4">
          <IoMdTime size={20} color="gray" />
          <p className="pl-1">{t("upper2")} </p>
        </div>

        <div className="flex flex-row items-center">
          <IoCallOutline size={20} color="gray" />
          <p className="pl-1">{t("upper3")}</p>
        </div>
      </div>
      <div className="flex flex-row items-center">
        <div className="flex flex-row items-center pr-4">
          <IoMailOpenOutline size={20} color="gray" />
          <p className="pl-1">{t("upper4")}</p>
        </div>
        <a
          href="https://www.facebook.com/profile.php?id=100069351849783"
          target={"_blank"}
          rel="noreferrer"
        >
          <BsFacebook size={20} color="white" className="mr-4" />
        </a>
        {/* <BsInstagram size={20} color="white" /> */}
      </div>
    </div>
  );
}
