export const ActiveBtnDot = () => {
  return (
    <div className="absolute top-2 -right-1">
      <span className="relative flex h-2 w-2">
        <span className="absolute animate-ping inline-flex h-full w-full rounded-full bg-sky-400 opacity-75"></span>
        <span className="inline-flex rounded-full h-2 w-2 bg-sky-500"></span>
      </span>
    </div>
  );
};
export const ActiveBtnDotQuo = () => {
  return (
    <div className="absolute top-1 right-1">
      <span className="relative flex h-2 w-2">
        <span className="absolute animate-ping inline-flex h-full w-full rounded-full bg-sky-50 opacity-75"></span>
        <span className="inline-flex rounded-full h-2 w-2 bg-sky-300"></span>
      </span>
    </div>
  );
};
