import React from "react";
import { NextSeo } from "next-seo";
import siteMetadata from "../data/meta.json";

export const SEO = {
  title: siteMetadata.title,
  description: siteMetadata.description,
  openGraph: {
    type: "website",
    locale: siteMetadata.language,
    url: siteMetadata.siteUrl,
    title: siteMetadata.title,
    description: siteMetadata.description,
    images: [
      {
        url: `${siteMetadata.socialBanner}`,
        alt: siteMetadata.title,
        width: 1200,
        height: 600,
      },
    ],
  },
  facebook: {
    handle: siteMetadata.facebook,
    site: siteMetadata.facebook,
    cardType: "summary_large_image",
  },
  additionalMetaTags: [
    {
      name: "author",
      content: siteMetadata.author,
    },
  ],
};

const Seo = ({
  title = siteMetadata.title,
  description = siteMetadata.description,
  url = siteMetadata.siteUrl,
  image = siteMetadata.image,
}) => {
  return (
    <NextSeo
      title={`${title} | ${siteMetadata.title}`}
      description={description}
      image={image}
      canonical={url}
      openGraph={{
        url,
        description,
        title: description,
        images: [{ url: image, width: 800, height: 500, alt: "Image Team" }],
      }}
    />
  );
};

export default Seo;
