import Link from "next/link";
import React from "react";

const CustomError = () => {
  return (
    <div className="w-screen h-screen flex flex-col justify-center items-center bg-bgdark gap-3 md:gap-4">
      <p className="w-2/3 text-center text-white md:text-2xl text-xl">
        Таны хайж буй хуудас манай вэб хуудсанд байхгүй байна.
      </p>
      <Link
        href="/"
        className="bg-bglight hover:bg-slate-700 transition-all ease-in-out text-white text-semibold rounded-md px-4 py-2 font-medium"
      >
        Нүүр хуудас
      </Link>
    </div>
  );
};

export default CustomError;
