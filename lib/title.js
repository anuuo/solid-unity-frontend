import React from "react";

const Title = (props) => {
  return (
    <p className="md:text-6xl sm:text-4xl text-3xl sm:px-0 px-10 pb-4  text-gray-50 text-center font-extralight uppercase">
      {props.children}
    </p>
  );
};
export const ScreenTitle = (props) => {
  return (
    <p className="md:text-6xl text-4xl text-gray-500 text-center font-medium tracking-wide ">
      {props.children}
    </p>
  );
};
export const ScreenTitleLg = (props) => {
  return (
    <p className="md:text-6xl text-4xl text-gray-400 text-center font-medium tracking-wide ">
      {props.children}
    </p>
  );
};

const SubTitle = ({ children, color = "slate-300", align = "center" }) => {
  return (
    <p className="md:text-5xl text-3xl pb-4 text-slate-800 font-extralight">
      {children}
    </p>
  );
};
const SubTitleLg = ({ children, align = "center" }) => {
  return (
    <p className="md:text-5xl text-4xl pb-3 text-white font-extralight capitalize">
      {children}
    </p>
  );
};

const SubTitleWhite = ({ children, color = "slate-300", align = "center" }) => {
  return (
    <p className="md:text-5xl text-3xl pb-8 text-slate-50 font-extralight">
      {children}
    </p>
  );
};

const MiniTitle = ({ children, color = "gray-300", align = "center" }) => {
  return (
    <p
      className={`lg:text-2xl text-xl tracking-wide text-${color} text-${align} font-light`}
    >
      {children}
    </p>
  );
};

export { Title, SubTitle, MiniTitle, SubTitleWhite, SubTitleLg };
