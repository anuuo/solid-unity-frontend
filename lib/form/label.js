import React from "react";

const Label = ({ req, text }) => {
  return (
    <div className="flex flex-row">
      {req && <p className="text-red-500 mr-[2px]">*</p>}
      <p className=" pb-1 text-lg text-gray-50">{text}</p>
    </div>
  );
};

export default Label;
