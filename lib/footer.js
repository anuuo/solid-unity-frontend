import Link from "next/link";
import React, { useTransition } from "react";
import { IoMailOpenOutline, IoCallOutline } from "react-icons/io5";
import { IoMdTime } from "react-icons/io";
// import { BsFacebook, BsInstagram, BsYoutube, BsTwitter } from "react-icons/bs";
import { useTranslation } from "next-i18next";

const CusLink = ({ href, name }) => {
  return (
    <Link href={href}>
      <p className="hover:text-sky-400 font-light text-slate-300/80 transition-all ease-in ">
        {name}
      </p>
    </Link>
  );
};

const FacebookPage = ({ title = "" }) => {
  return (
    <React.Fragment>
      <div>
        <span className="block uppercase text-blueGray-500 text-base font-semibold mb-2">
          {title}
        </span>
        <iframe
          src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fprofile.php%3Fid%3D100069351849783&tabs=timeline&width=300&height=200&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId"
          style={{
            border: "none",
            overflow: "hidden",
          }}
          // width={200}
          // height={100}
          scrolling="no"
          frameBorder="0"
          allowFullScreen={true}
          allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"
        ></iframe>
      </div>
    </React.Fragment>
  );
};

const Footer = () => {
  const { t } = useTranslation();
  return (
    <div className="relative">
      <div className="w-full lg:px-40 px-10 py-24 bg-[#000e1c] text-base font-thin text-gray-200 ">
        <div className="flex md:flex-row flex-col text-center lg:text-left justify-between gap-5 sm:gap-0 ">
          <div className="w-full lg:w-6/12 ">
            <h4 className="text-2xl  font-medium">{t("margins:title")}</h4>
            <h5 className="text-base my-2 text-blueGray-600 md:text-left text-center">
              {t("margins:desc")}
            </h5>
            <div className="flex flex-col md:items-start items-center">
              <div className="flex flex-row items-center pr-4">
                <IoMdTime size={20} color="gray" className="md:block hidden" />
                <p className="pl-2">{t("margins:upper2")}</p>
              </div>
              <div className="flex flex-row items-center pr-4">
                <IoMailOpenOutline
                  size={20}
                  color="gray"
                  className="md:block hidden"
                />
                <p className="pl-2">{t("margins:upper4")}</p>
              </div>
              <div className="flex flex-row items-center">
                <IoCallOutline
                  size={20}
                  color="gray"
                  className="md:block hidden"
                />
                <p className="pl-2">{t("margins:upper3")}</p>
              </div>
            </div>
          </div>

          <div className="w-full lg:w-6/12 px-4 md:my-0 my-6">
            <div className="flex flex-row w-full justify-center gap-10  items-start">
              <div className="">
                <span className="block uppercase text-blueGray-500 text-base font-medium mb-2">
                  {t("margins:link")}
                </span>
                <div className="flex flex-col gap-1">
                  <CusLink href={"/"} name={t("navigation:home")} />
                  <CusLink href={"aboutus"} name={t("navigation:aboutus")} />
                  <CusLink href={"service"} name={t("navigation:service")} />
                  <CusLink
                    href={"humanresource"}
                    name={t("navigation:humanresource")}
                  />
                  <CusLink href={"contact"} name={t("navigation:contact")} />
                </div>
              </div>
              <div className="">
                <span className="block uppercase text-blueGray-500 text-base font-semibold mb-2">
                  {t("margins:others")}
                </span>
                <div className="flex flex-col gap-1">
                  <CusLink href={"/certificate"} name={t("margins:cert")} />
                  <CusLink href={"/tos"} name={t("margins:tos")} />
                  <CusLink href={"/privacy"} name={t("margins:pp")} />
                </div>
              </div>
            </div>
          </div>
          <div className="md:flex flex-col lg:w-1/2 md:w-1/3 hidden  items-center">
            <FacebookPage title={t("margins:facebook")} />
          </div>
          <div className="flex-col w-full md:hidden flex items-center">
            <FacebookPage title={t("margins:facebook")} />
          </div>
        </div>
      </div>
      <div className="w-full lg:px-40 px-10 py-8 bg-gray-900 text-gray-400 text-base text-center font-light relative">
        <div className="text-xl  flex md:flex-row flex-col justify-center items-center">
          <p>
            Copyright © {new Date().getFullYear()} Solid Unity LLC - made by
          </p>
          <a
            href={"https://vicianu.tech/#/"}
            target="_blank"
            className="px-2 text-blue-100 font-semibold cursor-pointer uppercase hover:text-red-500 transition-all"
            rel="noreferrer"
          >
            Anu Otgonjargal
          </a>
        </div>
      </div>
    </div>
  );
};

export default Footer;
