import React from "react";
import { BsArrowUpCircleFill } from "react-icons/bs";
import { mergeNames } from "@/utils/index";

const ScrollTopButton = () => {
  const [scrolled, setScrolled] = React.useState(false);

  React.useEffect(() => {
    const handleScroll = () => {
      setScrolled(window.scrollY > 200);
    };
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  const handleClick = () => {
    if (typeof window !== "undefined") {
      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
    }
  };

  return (
    <div className="fixed bottom-5 sm:right-10 right-5 z-[100]">
      <button
        onClick={handleClick}
        className={mergeNames(
          "transition-all duration-400 ease-in-out",
          "bg-white rounded-full",
          !scrolled ? "opacity-0" : "opacity-100",
          !scrolled ? "translate-y-[100px]" : "translate-y-[0px]"
        )}
      >
        {/* <div className=""> */}
        <BsArrowUpCircleFill size={40} color="#334155" />
        {/* </div> */}
      </button>
    </div>
  );
};

export default ScrollTopButton;
