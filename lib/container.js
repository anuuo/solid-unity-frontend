export const LightContainer = (props) => {
  return (
    <div className="w-full lg:px-40 px-10 py-24 bg-bg text-gray-500 text-base">
      {props.children}
    </div>
  );
};

export const WhiteContainer = (props) => {
  return (
    <div className="w-full lg:px-40 md:px-30 px-10 py-24 text-center bg-white">
      {props.children}
    </div>
  );
};
