import Image from "next/image";

export default function Maintenance(props) {
  return (
    <>
      <div className="w-screen h-screen bg-slate-900 flex items-center justify-center flex-col">
        <div className="w-1/2 relative h-1/3 ">
          <Image
            src="/logo.png"
            // src=""
            fill
            style={{ objectFit: "contain" }}
            alt="Logo"
          />
        </div>
        <div className="sm:w-1/2 w-full sm:px-4 px-10 text-center space-y-4">
          <p className="text-slate-200 font-medium sm:text-5xl text-3xl ">
            Манай веб шинэчлэгдэж байна.
          </p>
          <p className="text-slate-400 font-medium sm:text-2xl text-lg">
            Та түр хүлээнэ үү. Баярлалаа.
          </p>
        </div>
      </div>
    </>
  );
}
