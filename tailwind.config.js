/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./lib/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      screens: {
        xs: "475px",
        "3xl": "1800px",
      },
      colors: {
        dark: "#05070c",
        primary: "#1890ff", // rgb(24, 144, 255)
        secondary: "rgb(0, 21, 41)", // 001529
        // bgdark: "#1e293b", //slate800
        // bgdark: "#0a1a2c", //slate800
        // bgdark: "#0a1a2c", //slate800
        // bgdark: "#1e293b", //slate800
        // bgdark: "#1e293b", //slate800
        // bglight: "#1d2c3e", //slate700
        bglight: "#0E2036",
        // bglight: "#1e293b",
        bgdark: "#001529", // rgb(0, 21, 41)
        // bgdark: "#132134",
        // bgdark: "#0d1d30",
      },
    },
  },
  plugins: [],
};
