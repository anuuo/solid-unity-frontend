const partnersdata = [
  {
    id: 0,
    name: "Org",
    img: "/partners/4.jpeg",
  },
  {
    id: 1,
    name: "Org",
    img: "/partners/5.png",
  },
  {
    id: 2,
    name: "Org",
    img: "/partners/1.png",
  },
  {
    id: 3,
    name: "Org",
    img: "/partners/2.png",
  },
  {
    id: 4,
    name: "Org",
    img: "/partners/3.png",
  },
  {
    id: 5,
    name: "Org",
    img: "/partners/6.jpeg",
  },
  {
    id: 6,
    name: "Org",
    img: "/partners/16.png",
  },
  {
    id: 7,
    name: "Org",
    img: "/partners/7.jpeg",
    link: "http://kars.ka-group.mn/",
  },
  {
    id: 8,
    name: "Org",
    img: "/partners/8.png",
    link: "http://kars.ka-group.mn/",
  },
  {
    id: 9,
    name: "Org",
    img: "/partners/9.png",
  },
  {
    id: 10,
    name: "Org",
    img: "/partners/11.png",
    link: "https://primaryenergy.mn/",
  },
  {
    id: 11,
    name: "Org",
    img: "/partners/12.jpeg",
  },
  {
    id: 12,
    name: "Org",
    img: "/partners/13.png",
  },
  {
    id: 13,
    name: "Org",
    img: "/partners/14.png",
  },
  {
    id: 14,
    name: "Org",
    img: "/partners/15.png",
  },
];

export default partnersdata;
